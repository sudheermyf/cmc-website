﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TouchonCloudSecurityDB.DBFolder;

namespace TocCMC
{
    public static class AppDB
    {
        public static TouchonCloudSecurityDBContext securityDB = new TouchonCloudSecurityDBContext();
        public static Logger Logger = LogManager.GetLogger("myfashionAPI");
    }
}