﻿using TouchonCloudSecurityDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TocCMC
{
    public partial class UserRegistration : System.Web.UI.Page
    {
        long inc = 0;
        string strinc;
        long strID;
        object lockTarget = new object();
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    GetTotalData();
                }
            }
        }

        private void GetTotalData()
        {
            lock (lockTarget)
            {
                AppDB.Logger.Info("Initiating Create User Registrations Or Store Admins.");
                AppDB.Logger.Info("Checking for Atleast One Application Name exists in DB..");
                if (AppDB.securityDB.RegApps.ToList().Count > 0)
                {
                    lock (lockTarget)
                    {
                        AppDB.Logger.Info("Fetching all the applications and bind to the dropdownlist");
                        ddlApp.DataSource = AppDB.securityDB.RegApps.ToList();
                        ddlApp.DataBind();
                    }

                }
                lock (lockTarget)
                {
                    if (AppDB.securityDB.StoreAdmin.ToList().Count > 0)
                        strinc = AppDB.securityDB.StoreAdmin.Max(c => c.UserID);
                    inc = Convert.ToInt64(strinc);
                    if (inc > 0)
                    {
                        strID = inc + 1;
                    }
                    else
                    {
                        strID = 1;
                    }
                    txtuserid.Text = strID.ToString();
                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (AppDB.securityDB.StoreAdmin.ToList().Count > 0)
                strinc = AppDB.securityDB.StoreAdmin.Max(c => c.UserID);
            inc = Convert.ToInt64(strinc);
            if (inc > 0)
            {
                strID = inc + 1;
            }
            else
            {
                strID = 1;
            }
            txtuserid.Text = strID.ToString();
            AppDB.Logger.Info("Initiating Post Store Admin " + txtUserName.Text + " Started.");
            if (!string.IsNullOrEmpty(txtUserName.Text.Trim()) && !string.IsNullOrEmpty(txtPassword.Text.Trim()) && !string.IsNullOrEmpty(txtuserid.Text.Trim()) && ddlApp != null && !string.IsNullOrEmpty(ddlApp.SelectedItem.Text) && ddlConfig != null && !string.IsNullOrEmpty(ddlConfig.SelectedItem.Text))
            {
                AppDB.Logger.Info("Checking user exist in DB.");
                if (!AppDB.securityDB.StoreAdmin.ToList().Exists(c => c.UserName.Equals(txtUserName.Text.Trim(), StringComparison.OrdinalIgnoreCase)))
                {
                    try
                    {
                        AppDB.Logger.Info("Checking Registered Application Exist in DB.");
                        lock (lockTarget)
                        {
                            TouchonCloudSecurityDB.Models.StoreAdmin accessusers = new TouchonCloudSecurityDB.Models.StoreAdmin();
                            accessusers.RegApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlApp.SelectedItem.Text)).FirstOrDefault();
                            accessusers.CMCConfig = AppDB.securityDB.CMCConfig.Where(c => c.ConfigurationName.Equals(ddlConfig.SelectedItem.Text)).FirstOrDefault();
                            if (AppDB.securityDB.RegApps.ToList().Exists(c => c.SNo.Equals(accessusers.RegApp.SNo)) && AppDB.securityDB.CMCConfig.ToList().Exists(c => c.SNo.Equals(accessusers.CMCConfig.SNo)))
                            {
                                accessusers.UserID = strID.ToString();
                                accessusers.UserName = txtUserName.Text.Trim();
                                accessusers.Password = txtPassword.Text.Trim();
                                accessusers.StoreName = txtStoreName.Text.Trim();
                                accessusers.StoreEMail = txtStoreEmail.Text.Trim();
                                accessusers.StoreContactNumber = txtStoreContactNumber.Text;
                                accessusers.RegApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlApp.SelectedItem.Text)).FirstOrDefault();
                                accessusers.IsActive = true;
                                accessusers.IsUpdateAvailable = true;
                                accessusers.CMCConfig = AppDB.securityDB.CMCConfig.Where(c => c.ConfigurationName.Equals(ddlConfig.SelectedItem.Text)).FirstOrDefault();
                                accessusers.UpdatedTime = DateTime.Now;
                                AppDB.securityDB.StoreAdmin.Add(accessusers);
                                AppDB.securityDB.SaveChanges();
                                ClientScript.RegisterStartupScript(GetType(), "notifier", "successAlert('Store Admin Has Been Created Successfully');", true);
                                
                                
                                clearAll();
                            }
                            else
                            {
                                AppDB.Logger.Info("Attached properties are empty.");
                                ClientScript.RegisterStartupScript(GetType(), "notifier1", "errorAlert('Attached properties are empty');", true);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        AppDB.Logger.ErrorException(ex.Message, ex);
                        ClientScript.RegisterStartupScript(GetType(), "notifier2", "errorAlert('" + ex.Message + "');", true);
                    }

                }
                else
                {
                    AppDB.Logger.Info("Store Admin already registered. Please choose another name.");
                    ClientScript.RegisterStartupScript(GetType(), "notifier3", "errorAlert('Store Admin already registered. Please choose another name');", true);
                }
            }
            else
            {
                AppDB.Logger.Info("Required fields should not be empty.");
                ClientScript.RegisterStartupScript(GetType(), "notifier4", "errorAlert('Required fields should not be empty');", true);
            }
        }
        private void clearAll()
        {
            ddlApp.ClearSelection();
            ddlApp.Items.FindByText("Select").Selected = true;
            ddlConfig.ClearSelection();
            ddlConfig.Items.FindByText("Select").Selected = true;
            txtStoreName.Text = "";
            txtStoreEmail.Text = "";
            txtStoreContactNumber.Text = "";
            txtUserName.Text = "";
            txtPassword.Text = "";
        }
        protected void ddlApp_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlApp.SelectedItem.Text != "Select")
            {
                lock (lockTarget)
                {

                    RegisteredApplications selectedApplication = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlApp.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (selectedApplication != null && selectedApplication.LstCMCConfig.Count > 0)
                    {
                        ddlConfig.Items.Clear();
                        ddlConfig.Items.Add("Select");
                        ddlConfig.DataSource = selectedApplication.LstCMCConfig.ToList();
                        ddlConfig.DataBind();
                    }
                    else
                    {
                        ddlConfig.Items.Clear();
                        ddlConfig.Items.Add("Select");
                    }
                }
            }
            else
            {
                ddlConfig.Items.Clear();
                ddlConfig.Items.Add("Select");
            }

        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            clearAll();
            ClientScript.RegisterStartupScript(GetType(), "notifier5", "messageAlert('Data has been reset');", true);
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            clearAll();
            Response.Redirect("ViewUsers.aspx");
        }
    }
}