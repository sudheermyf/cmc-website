﻿using TouchonCloudSecurityDB.Models;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TocCMC
{
    public partial class ViewConfidentialKeys : System.Web.UI.Page
    {
        object lockTarget = new object();
        DataTable dt, dtSearch;
       
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    lock (lockTarget)
                    {
                        AppDB.Logger.Info("Initiating View CMC.");
                        //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                        AppDB.Logger.Info("Checking for Atleast One Configuration exists in DB..");
                        if (AppDB.securityDB.ConfidentialKeys.ToList().Count > 0)
                        {
                            GetTotalData();
                            ddlappname.DataSource = dt;
                            ddlappname.DataBind();
                        }
                    }
                }
            }
        }

        private void GetTotalData()
        {
            lock (lockTarget)
            {
                dt = new DataTable();
                DataRow dr = null;
                dt.Columns.Add(new DataColumn("strSno", typeof(long)));
                dt.Columns.Add(new DataColumn("strAppName", typeof(string)));
                dt.Columns.Add(new DataColumn("strkey", typeof(string)));
                dt.Columns.Add(new DataColumn("keyid", typeof(long)));
                foreach (var item in AppDB.securityDB.ConfidentialKeys.ToList())
                {
                    long strSno = item.SNo;
                    string strkey = item.SecretKey;
                    long keyid = item.KeyID;
                    lock (lockTarget)
                    {
                        TouchonCloudSecurityDB.Models.RegisteredApplications regAppName = AppDB.securityDB.RegApps.Where(c => c.ConfidentialKey.SNo.Equals(item.SNo)).FirstOrDefault();
                        if (regAppName != null)
                        {
                            string strAppName = regAppName.AppName;
                            dr = dt.NewRow();
                            dr["strSno"] = strSno;
                            dr["strAppName"] = strAppName;
                            dr["strkey"] = strkey;
                            dr["keyid"] = keyid;
                            dt.Rows.Add(dr);
                            //Store the DataTable in ViewState
                            ViewState["CurrentTable"] = dt;
                        }
                    }
                }
                gdviewcnfkey.DataSource = dt;
                gdviewcnfkey.DataBind();
                AppDB.Logger.Info("Fetching all the confidential key from DB and binding to the Gridview");
            }
        }
        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreateCnfKey.aspx");
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            AppDB.Logger.Info("Initiating Update Confidential Key... Key ID: " + lblid.Text + " Key Name: " + txtkeyid.Text);
            try
            {
                if (Convert.ToInt64(txtkeyid.Text) > 0 && Convert.ToInt64(lblid.Text) > 0 && !string.IsNullOrEmpty(txtsecreykey.Text))
                {
                    if (AppDB.securityDB.ConfidentialKeys.ToList().Exists(c => c.SNo.Equals(Convert.ToInt64(lblid.Text))))
                    {
                        long id = Convert.ToInt64(lblid.Text);
                        lock (lockTarget)
                        {
                            ConfidentialKey filteredConfKey = AppDB.securityDB.ConfidentialKeys.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                            if (filteredConfKey.KeyID.Equals(Convert.ToInt64(txtkeyid.Text)))
                            {
                                filteredConfKey.KeyID = Convert.ToInt64(txtkeyid.Text);
                                filteredConfKey.SecretKey = txtsecreykey.Text;
                                AppDB.securityDB.SaveChanges();
                                GetTotalData();
                                ClientScript.RegisterStartupScript(GetType(), "notifier", "successAlert('Confidential Key Updated Successfully');", true);
                                
                                
                            }
                            else
                            {
                                AppDB.Logger.Info("Key IDs not matched.");
                                ClientScript.RegisterStartupScript(GetType(), "notifier1", "errorAlert('Key IDs not matched');", true);
                            }
                        }
                    }
                    else
                    {
                        AppDB.Logger.Info("ID not found.");
                        ClientScript.RegisterStartupScript(GetType(), "notifier2", "errorAlert('ID not found');", true);
                    }
                }
                else
                {
                    AppDB.Logger.Info("Invalid confidential data.");
                    ClientScript.RegisterStartupScript(GetType(), "notifier3", "errorAlert('Invalid confidential data');", true);
                }
            }
            catch (Exception ex)
            {
                AppDB.Logger.ErrorException(ex.Message, ex);
            }
        }

        private void GetSelectedItemData()
        {
            lock (lockTarget)
            {
                if (lblid.Text != null)
                {
                    int Index;
                    DataTable dt = (DataTable)ViewState["CurrentTable"];

                    for (int i = 0; i < gdviewcnfkey.Rows.Count; i++)
                    {
                        GridViewRow row = gdviewcnfkey.Rows[i];
                        RadioButton rdb = (RadioButton)row.FindControl("RadioButton1");
                        if (rdb.Checked == true)
                        {
                            Index = row.RowIndex;
                            lblid.Text = dt.Rows[Index]["strSno"].ToString();
                            txtkeyid.Text = dt.Rows[Index]["keyid"].ToString();
                            txtsecreykey.Text = dt.Rows[Index]["strkey"].ToString();
                            txtappname.Text = dt.Rows[Index]["strAppName"].ToString();
                        }
                    }
                }
            }
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Label1.Visible = false;
            bool isChecked = false;
            foreach (GridViewRow row in gdviewcnfkey.Rows)
            {
                RadioButton rb = (RadioButton)row.FindControl("RadioButton1");
                if (rb.Checked)
                {
                    isChecked = true;
                }
            }

            if (!isChecked)
            {
                ClientScript.RegisterStartupScript(GetType(), "notifier4", "warningAlert('Please Select Anyone');", true);
            }
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<script type='text/javascript'>");
                sb.Append("$('#myModalCnfKey').modal('show');");
                sb.Append("</script>");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", sb.ToString(), false);
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Label1.Visible = false;
            int Index;
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            long id = 0;
            for (int i = 0; i < gdviewcnfkey.Rows.Count; i++)
            {
                GridViewRow row = gdviewcnfkey.Rows[i];
                RadioButton rb = (RadioButton)row.FindControl("RadioButton1");
                if (rb.Checked)
                {
                    Index = row.RowIndex;
                    //int strI = dt.Rows.IndexOf(dt.Rows[Index]);
                    string strId = dt.Rows[Index]["strSno"].ToString();
                    id = Convert.ToInt64(strId);
                    try
                    {
                        AppDB.Logger.Info("Initiating Delete Confidential key... Key ID: " + id);
                        if (AppDB.securityDB.ConfidentialKeys.ToList().Exists(c => c.SNo.Equals(id)))
                        {
                            TouchonCloudSecurityDB.Models.ConfidentialKey GetConfidentialKey = AppDB.securityDB.ConfidentialKeys.Where(c => c.SNo.Equals(id)).FirstOrDefault();

                            TouchonCloudSecurityDB.Models.RegisteredApplications reg = AppDB.securityDB.RegApps.Where(c => c.ConfidentialKey.SecretKey.Equals(GetConfidentialKey.SecretKey)).FirstOrDefault();
                            if (reg != null)
                            {
                                lock (lockTarget)
                                {
                                    TouchonCloudSecurityDB.Models.SecurityKey secKey = AppDB.securityDB.SecurityKey.Where(c => c.ApplicationName.Equals(reg.AppName)).FirstOrDefault();

                                    if (secKey == null)
                                    {
                                        AppDB.Logger.Info("Confidential key exist... Initiating Delete process.");
                                        ConfidentialKey confKey = AppDB.securityDB.ConfidentialKeys.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                                        AppDB.securityDB.ConfidentialKeys.Remove(confKey);
                                        AppDB.securityDB.SaveChanges();
                                        GetTotalData();
                                        ClientScript.RegisterStartupScript(GetType(), "notifier5", "successAlert('Confidential Key Deleted Successfully');", true);
                                        
                                        
                                        AppDB.Logger.Info("Delete Successfully.");
                                    }
                                    else
                                    {
                                        ClientScript.RegisterStartupScript(GetType(), "notifier6", "warningAlert('First Delete Security Keys related to this Confidential Key');", true);
                                    }
                                }

                            }
                        }
                        else
                        {
                            AppDB.Logger.Info("Invalid ID.");
                            ClientScript.RegisterStartupScript(GetType(), "notifier7", "errorAlert('Invalid ID');", true);
                        }
                    }
                    catch (Exception ex)
                    {
                        AppDB.Logger.ErrorException(ex.Message, ex);
                        ClientScript.RegisterStartupScript(GetType(), "notifier8", "errorAlert('" + ex.Message + "');", true);
                    }
                }
            }
        }
        protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            GetSelectedItemData();
        }
        protected void gdviewcnfkey_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dataTable = ViewState["CurrentTable"] as DataTable;
            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression;
                gdviewcnfkey.DataSource = dataView;
                gdviewcnfkey.DataBind();
            }
        }
        protected void ddlappname_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlappname.SelectedItem.Text != "Select")
            {
                lock (lockTarget)
                {
                    gdviewcnfkey.DataSource = null;
                    dtSearch = new DataTable();
                    DataRow dr1 = null;
                    dtSearch.Columns.Add(new DataColumn("strSno", typeof(long)));
                    dtSearch.Columns.Add(new DataColumn("strAppName", typeof(string)));
                    dtSearch.Columns.Add(new DataColumn("strkey", typeof(string)));
                    dtSearch.Columns.Add(new DataColumn("keyid", typeof(long)));

                    TouchonCloudSecurityDB.Models.RegisteredApplications regAppName = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text)).FirstOrDefault();
                    if (regAppName != null)
                    {
                        string strAppName = regAppName.AppName;
                        dr1 = dtSearch.NewRow();
                        dr1["strSno"] = regAppName.ConfidentialKey.SNo.ToString();
                        dr1["strAppName"] = strAppName;
                        dr1["strkey"] = regAppName.ConfidentialKey.SecretKey;
                        dr1["keyid"] = regAppName.ConfidentialKey.KeyID.ToString();
                        dtSearch.Rows.Add(dr1);
                        //Store the DataTable in ViewState
                        ViewState["CurrentTable"] = dtSearch;
                    }

                    gdviewcnfkey.DataSource = dtSearch;
                    gdviewcnfkey.DataBind();
                    AppDB.Logger.Info("Fetching all the confidential key from DB and binding to the Gridview");
                }
            }
            else
            {
                GetTotalData();
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            GetSelectedItemData();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type='text/javascript'>");
            sb.Append("$('#myModalCnfKey').modal('show');");
            sb.Append("</script>");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", sb.ToString(), false);
        }
    }
}