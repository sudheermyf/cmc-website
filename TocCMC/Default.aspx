﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TocCMC.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TOC CMC</title>    
    <link href="<%=Page.ResolveUrl("~/css/bootstrap-theme.min.css") %>" rel="stylesheet" />
    <link href="<%=Page.ResolveUrl("~/css/bootstrap.min.css") %>" rel="stylesheet" />
    <link href="<%=Page.ResolveUrl("~/css/alertify.css") %>" rel="stylesheet" />
    <link href="<%=Page.ResolveUrl("~/css/alertify.min.css") %>" rel="stylesheet" />
    <link href="<%=Page.ResolveUrl("~/css/themes/default.rtl.css") %>" rel="stylesheet" />
    <link href="<%=Page.ResolveUrl("~/css/customstyle.css") %>" rel="stylesheet" />
    <script src="<%=Page.ResolveUrl("~/js/alertify.js") %>"></script>
    <script src="<%=Page.ResolveUrl("~/js/alertify.min.js") %>"></script>
    <!-- JavaScript -->
	<script src="<%=Page.ResolveUrl("~/js/jquery-1.10.2.js") %>"></script>
    <script src="<%=Page.ResolveUrl("~/js/bootstrap.min.js") %>"></script>
	<script src="<%=Page.ResolveUrl("~/js/modern-business.js") %>"></script>
	
    <script type="text/javascript">
        function successAlert(msg) {
            alertify.success(msg);
        }
        function warningAlert(msg) {
            alertify.warning(msg);
        }
        function errorAlert(msg) {
            alertify.error(msg);
        }
        function messageAlert(msg) {
            alertify.message(msg);
        }
    </script>

    <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script>
        $(document).ready(function () {
            $(".Username").keyup(function () {
                var Username = $(".Username").val();    //gets the value of class Username
                if (Username == "")     //Username should not be empty
                {
                    $(".UsernameErr").text("please enter username");
                    return false;
                }
                if(Username.length > 40)    //username length should not exceed 40 characters
                {
                    $(".UsernameErr").text("username should not exceed 40 characters");
                    return false;
                }
                else {
                    $(".UsernameErr").text("");
                }
            });
            $(".Password").keyup(function () {
                var Password = $(".Password").val();    //gets the value of class Password
                if (Password == "")     //password should not be empty
                {
                    $(".PasswordErr").text("please enter password");
                    return false;
                }
                if (Password.length < 6)    //password minimum length is 6 characters
                {
                    $(".PasswordErr").text("minimum 6 characters");
                    return false;
                }
                if (Password.length > 16)   //password maximum length is 16 characters
                {
                    $(".PasswordErr").text("maximum 16 characters");
                    return false;
                }
                else {
                    $(".PasswordErr").text("");
                }
            });
            $(".Validate").click(function () {
                var count = 0;
                var Username = $(".Username").val();
                var Password = $(".Password").val();
                if (Username == "") {
                    $(".UsernameErr").text("please enter username");
                    count = count + 1;
                }
                if (Username.length > 40)    //username length should not exceed 40 characters
                {
                    $(".UsernameErr").text("username should not exceed 40 characters");
                    count = count + 1;
                }
                if (Password.length < 6)    //password minimum length is 6 characters
                {
                    $(".PasswordErr").text("minimum 6 characters");
                    count = count + 1;
                }
                if (Password.length > 16)   //password maximum length is 16 characters
                {
                    $(".PasswordErr").text("maximum 16 characters");
                    count = count + 1;
                }
                if (Password == "") {
                    $(".PasswordErr").text("please enter password");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" role="form" autocomplete="off" >        
        <div class="col-xs-6 col-sm-6 col-md-3 jumbotron col-centered" style="margin-top:12%;">
            <div class="form-group">
                <asp:TextBox ID="txtusername" class="form-control Username" placeholder="Username" runat="server"/>
                <span class="UsernameErr error-code"></span>
            </div>
            <div class="form-group">
                <asp:TextBox ID="txtpwd" class="form-control Password" runat="server" oncopy="return false" onpaste="return false"
                    oncut="return false" placeholder="Password" TextMode="Password"/>
                <span class="PasswordErr error-code"></span>
            </div>
            <div class="form-group">
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" class="btn btn-primary btn-md btn-block Validate" Text="Submit" />
                <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" class="btn btn-default btn-md btn-block" Text="Cancel" />
            </div>
        </div>
    </form>
</body>

</html>
