﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="UserRegistration.aspx.cs" Inherits="TocCMC.UserRegistration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script type="text/javascript">
        function Reset() {
            $(".ddlApp").get(0).selectedIndex = 0;
            $(".ddlConfigName").get(0).selectedIndex = 0;
            $(".storename").val("");
            $(".emailid").val("");
            $(".username").val("");
            $(".password").val("");
            $(".contactnumber").val("");
            return true;
        }

        $(document).ready(function () {
            $(".ddlApp").change(function () {
                var ddlApp = $(".ddlApp").val();
                if (ddlApp == "Select") {
                    $(".ddlAppErr").text("please select application");
                    return false;
                }
                else {
                    $(".ddlAppErr").text("");
                }
            });
            $(".ddlConfigName").change(function () {
                var ddlConfigName = $(".ddlConfigName").val();
                if (ddlConfigName == "Select") {
                    $(".ddlConfigNameErr").text("please select configuration name");
                    return false;
                }
                else {
                    $(".ddlConfigNameErr").text("");
                }
            });
            $(".storename").keyup(function () {
                var storename = $(".storename").val();    //gets the value of class storename
                //var appexpression = /^[0-9a-zA-Z ]+$/;
                if (storename == "")     //storename should not be empty
                {
                    $(".storenameErr").text("please enter store name");
                    return false;
                }
                //if (!appexpression.test(storename)) {
                //    $(".storenameErr").text("special characters, numbers not allowed");
                //    return false;
                //}
                if (storename.length < 3)    //storename minimum length is 3 characters
                {
                    $(".storenameErr").text("minimum 3 characters");
                    return false;
                }
                if (storename.length > 30)   //storename maximum length is 30 characters
                {
                    $(".storenameErr").text("maximum 30 characters");
                    return false;
                }
                else {
                    $(".storenameErr").text("");
                }
            });
            $(".emailid").keyup(function () {
                var emailid = $(".emailid").val();    //gets the value of class username
                if (emailid == "")     //username should not be empty
                {
                    $(".emailidErr").text("please enter email id");
                    return false;
                }
                else {
                    $(".emailidErr").text("");
                }
            });
            $(".username").keyup(function () {
                var username = $(".username").val();    //gets the value of class username
                if (username == "")     //username should not be empty
                {
                    $(".usernameErr").text("please enter username name");
                    return false;
                }
                if (username.length > 40)   //username maximum length is 40 characters
                {
                    $(".usernameErr").text("maximum 40 characters");
                    return false;
                }
                else {
                    $(".usernameErr").text("");
                }
            });
            $(".password").keyup(function () {
                var password = $(".password").val();    //gets the value of class password                
                if (password == "")     //password should not be empty
                {
                    $(".passwordErr").text("please enter password");
                    return false;
                }
                if (password.length < 6)    //password minimum length is 6 characters
                {
                    $(".passwordErr").text("minimum 6 characters");
                    return false;
                }
                if (password.length > 16)   //password maximum length is 16 characters
                {
                    $(".passwordErr").text("maximum 16 characters");
                    return false;
                }
                else {
                    $(".passwordErr").text("");
                }
            });
            $(".contactnumber").keyup(function () {
                var contactnumber = $(".contactnumber").val();    //gets the value of class contactnumber  
                var contactexpression = /^[789][0-9]{9}$/;
                ///^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1}){0,1}98(\s){0,1}(\-){0,1}(\s){0,1}[1-9]{1}[0-9]{7}$/;;
                if (!contactexpression.test(contactnumber)) {
                    $(".contactnumberErr").text("enter valid phone number");
                    return false;
                }
                if (contactnumber == "")     //contactnumber should not be empty
                {
                    $(".contactnumberErr").text("please enter contactnumber");
                    return false;
                }
                else {
                    $(".contactnumberErr").text("");
                }
            });
            $(".validate").click(function () {
                var count = 0;
                var ddlApp = $(".ddlApp").val();
                var ddlConfigName = $(".ddlConfigName").val();
                var storename = $(".storename").val();
                var username = $(".username").val();
                var password = $(".password").val();
                var emailid=$(".emailid").val();
                var contactnumber = $(".contactnumber").val();

                if (ddlApp == "Select") {
                    $(".ddlAppErr").text("please select application");
                    count = count + 1;
                }
                if (ddlConfigName == "Select") {
                    $(".ddlConfigNameErr").text("please select configuration name");
                    count = count + 1;
                }
                if(emailid == "")
                {
                    $(".emailidErr").text("please enter store email id");
                    count = count + 1;
                }
                //var appexpression = /^[0-9a-zA-Z ]+$/;
                //if (!appexpression.test(storename)) {
                //    $(".storenameErr").text("special characters, numbers not allowed");
                //    count = count + 1;
                //}
                if (storename.length < 3)    //storename minimum length is 3 characters
                {
                    $(".storenameErr").text("minimum 3 characters");
                    count = count + 1;
                }
                if (storename.length > 30)   //storename maximum length is 30 characters
                {
                    $(".storenameErr").text("maximum 30 characters");
                    count = count + 1;
                }
                if (storename == "")     //storename should not be empty
                {
                    $(".storenameErr").text("please enter store name");
                    count = count + 1;
                }
                if (username.length > 40)   //username maximum length is 40 characters
                {
                    $(".usernameErr").text("maximum 40 characters");
                    count = count + 1;
                }
                if (username == "") {
                    $(".usernameErr").text("please enter username");
                    count = count + 1;
                }
                if (password.length < 6)    //password minimum length is 6 characters
                {
                    $(".passwordErr").text("minimum 6 characters");
                    count = count + 1;
                }
                if (password.length > 16)   //password maximum length is 16 characters
                {
                    $(".passwordErr").text("maximum 16 characters");
                    count = count + 1;
                }
                if (password == "") {
                    $(".passwordErr").text("please enter password");
                    count = count + 1;
                }
                var contactexpression = /^[789][0-9]{9}$/;
                if (!contactexpression.test(contactnumber)) {
                    $(".contactnumberErr").text("enter valid phone number");
                    count = count + 1;
                }
                if (contactnumber == "")     //contactnumber should not be empty
                {
                    $(".contactnumberErr").text("please enter contactnumber");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>

    <div class="col-md-6 col-sm-5 col-centered" style="margin-top: 2%;">
        <div class="panel panel-info">
            <div class="panel-heading">User Registration</div>
            <div class="panel-body">

                <div class="row form-group" id="selectApp">
                    <div class="col-md-3 col-sm-3">Select Application</div>
                    <div class="col-md-6 col-sm-6">
                        <asp:DropDownList ID="ddlApp" runat="server" AppendDataBoundItems="true" class="form-control ddlApp" AutoPostBack="true" DataTextField="AppName"
                            DataValueField="SNo" OnSelectedIndexChanged="ddlApp_SelectedIndexChanged">
                            <asp:ListItem>Select</asp:ListItem>
                        </asp:DropDownList>
                        <span class="ddlAppErr error-code"></span>
                    </div>
                </div>

                <div class="row form-group" id="selectConfigName">
                    <div class="col-md-3 col-sm-3">Select Configuration Name</div>
                    <div class="col-md-6 col-sm-6">
                        <asp:DropDownList ID="ddlConfig" runat="server" AppendDataBoundItems="true" class="form-control ddlConfigName" AutoPostBack="true"
                            DataTextField="ConfigurationName" DataValueField="SNo">
                            <asp:ListItem>Select</asp:ListItem>
                        </asp:DropDownList>
                        <span class="ddlConfigNameErr error-code"></span>
                    </div>
                </div>
                <div class="row form-group" id="storeName">
                    <div class="col-md-3 col-sm-3">Store Name</div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtStoreName" runat="server" 
                            class="form-control storename"/>
                        <span class="storenameErr error-code"></span>
                    </div>
                </div>
                <div class="row form-group" id="storeEmail">
                    <div class="col-md-3 col-sm-3">Store Email</div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtStoreEmail" runat="server"                             
                            ToolTip="Enter Valid E-Mail" class="form-control emailid" />
                        <span class="emailidErr error-code"></span>
                    </div>
                    <%--pattern="^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z][a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$"--%>
                </div>
                <div class="row form-group" id="contactNum">
                    <div class="col-md-3 col-sm-3">Store Contact Number</div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtStoreContactNumber" runat="server" ToolTip="Enter only 10 digit mobile number"
                            class="form-control contactnumber" />
                        <span class="contactnumberErr error-code"></span>
                        <%--pattern="[789][0-9]{9}"--%> 
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 col-sm-3">UserID</div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtuserid" runat="server" ReadOnly="true" class="form-control" />
                    </div>
                </div>
                <div class="row form-group" id="userName">
                    <div class="col-md-3 col-sm-3">User Name</div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtUserName" runat="server" class="form-control username"></asp:TextBox>
                        <span class="usernameErr error-code"></span>
                        <%--<asp:RegularExpressionValidator ID="rev1" runat="server" ControlToValidate="txtUserName" ErrorMessage="Please Enter Valid UserName" ForeColor="Red" Font-Size="Smaller" ValidationExpression="^[a-zA-Z0-9_@.-]{3,20}$" Display="Dynamic"></asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtUserName" ID="RegularExpressionValidator3" ForeColor="Red" Font-Size="Smaller" ValidationExpression="^[\s\S]{3,}$" runat="server" ErrorMessage="Minimum 3  characters required."></asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtUserName" ID="RegularExpressionValidator4" ForeColor="Red" Font-Size="Smaller" ValidationExpression="^[\s\S]{0,20}$" runat="server" ErrorMessage="Limit Should Not Exceed 20 characters."></asp:RegularExpressionValidator>--%>
                    </div>
                </div>
                <div class="row form-group" id="password">
                    <div class="col-md-3 col-sm-3">Password</div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" class="form-control password"></asp:TextBox>
                        <span class="passwordErr error-code"></span>
                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic" ControlToValidate="txtPassword" ErrorMessage="Password must contain one of @#$%^&*/." ForeColor="Red" Font-Size="Smaller" ValidationExpression=".*[@#$%^&*/].*" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic" ControlToValidate="txtPassword" ErrorMessage="Password must be 4-16 nonblank characters." ForeColor="Red" Font-Size="Smaller" ValidationExpression="[^\s]{4,16}" />--%>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 col-sm-3">
                        <asp:Label ID="lblenabled" runat="server" Text="User Status" Visible="false"></asp:Label>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:RadioButtonList ID="radList" runat="server" Width="300px" Visible="false" RepeatColumns="2" RepeatDirection="Horizontal" Height="20px">
                            <asp:ListItem>Enable</asp:ListItem>
                            <asp:ListItem>Disable</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 col-sm-3">
                        <asp:Label ID="lblupdateavail" runat="server" Text="Update Status" Visible="false"></asp:Label>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:RadioButtonList ID="radupdate" runat="server" Width="300px" Visible="false" RepeatColumns="2" RepeatDirection="Horizontal" Height="20px">
                            <asp:ListItem>Active</asp:ListItem>
                            <asp:ListItem>Inactive</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 col-sm-3"></div>
                    <div class="col-md-6 col-sm-6">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-primary validate" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnReset" runat="server" Text="Reset" class="btn btn-default"
                            OnClientClick="javascript:return Reset()" 
                            OnClick="btnReset_Click"/>
                        <asp:Button ID="btnBack" runat="server" Text="Back" class="btn btn-default"
                            OnClientClick="javascript:return Reset()" 
                            OnClick="btnBack_Click" />
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
