﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TocCMC
{
    public partial class AppUpdate : System.Web.UI.Page
    {
        object lockTarget = new object();
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            AppDB.securityDB = new TouchonCloudSecurityDB.DBFolder.TouchonCloudSecurityDBContext();
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    lock (lockTarget)
                    {
                        AppDB.Logger.Info("Initiating App Update.");
                        //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                        AppDB.Logger.Info("Checking for Atleast One Application Name exists in DB..");
                        if (AppDB.securityDB.RegApps.ToList().Count > 0)
                        {
                            AppDB.Logger.Info("Fetching all the applications and bind to the dropdownlist");
                            ddlappname.DataSource = AppDB.securityDB.RegApps.ToList();
                            ddlappname.DataBind();
                        }
                    }
                }
            }

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (chkupdates.Checked == true)
            {
                AppDB.Logger.Info("Initiating Submit Button when IsUpdateAvailable");
                lock (lockTarget)
                {
                    if (ddlappname.SelectedItem.Text != "Select")
                    {
                        AppDB.Logger.Info("Updating App Service: " + ddlappname.SelectedItem.Text.Trim() + " Started.");
                        AppDB.Logger.Info("Fetching the Registered Application according to the selected..ddlappname " + ddlappname.SelectedItem.Text + "");
                        lock (lockTarget)
                        {
                            TouchonCloudSecurityDB.Models.RegisteredApplications regapp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                            regapp.IsUpdateAvailable = true;
                        }
                        lock (lockTarget)
                        {
                            AppDB.Logger.Info("Fetching all the StoreAdmins according to the selected..ddlappname " + ddlappname.SelectedItem.Text + "");
                            List<TouchonCloudSecurityDB.Models.StoreAdmin> StoreAdmin = AppDB.securityDB.StoreAdmin.Where(c => c.RegApp.AppName.Equals(ddlappname.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
                            foreach (var item in StoreAdmin)
                            {
                                item.IsUpdateAvailable = true;
                                AppDB.Logger.Info("Fetching all the  security keys according to the selected..Store Admins " + item.UserName + "");
                                lock (lockTarget)
                                {
                                    List<TouchonCloudSecurityDB.Models.SecurityKey> secKey = AppDB.securityDB.SecurityKey.Where(c => c.StoreAdmin.UserName.Equals(item.UserName)).ToList();
                                    foreach (var item1 in secKey)
                                    {
                                        item1.IsUpdateAvailable = true;
                                    }
                                }
                            }
                            AppDB.securityDB.SaveChanges();
                            
                            
                        }

                        AppDB.Logger.Info("AppUpdate Updated Successfully");
                        clearAll();
                        ClientScript.RegisterStartupScript(GetType(), "notifier", "successAlert('AppUpdate Updated Succesffully');", true);
                    }
                    else
                    {
                        AppDB.Logger.Info("Required fields should not be empty.");
                        ClientScript.RegisterStartupScript(GetType(), "notifier1", "warningAlert('Required fields should not be empty');", true);
                    }
                }

            }
            else
            {
                AppDB.Logger.Info("Initiating Submit Button when Not IsUpdateAvailable");
                lock (lockTarget)
                {
                    if (ddlappname.SelectedItem.Text != "Select")
                    {
                        AppDB.Logger.Info("Updating App Service: " + ddlappname.SelectedItem.Text.Trim() + " Started.");
                        AppDB.Logger.Info("Fetching the Registered Application according to the selected..ddlappname " + ddlappname.SelectedItem.Text + "");
                        lock (lockTarget)
                        {
                            TouchonCloudSecurityDB.Models.RegisteredApplications regapp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                            regapp.IsUpdateAvailable = false;
                        }
                        lock (lockTarget)
                        {
                            AppDB.Logger.Info("Fetching all the StoreAdmins according to the selected..ddlappname " + ddlappname.SelectedItem.Text + "");
                            List<TouchonCloudSecurityDB.Models.StoreAdmin> StoreAdmin = AppDB.securityDB.StoreAdmin.Where(c => c.RegApp.AppName.Equals(ddlappname.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
                            foreach (var item in StoreAdmin)
                            {
                                item.IsUpdateAvailable = false;
                                AppDB.Logger.Info("Fetching all the  security keys according to the selected..Store Admins " + item.UserName + "");
                                lock (lockTarget)
                                {
                                    List<TouchonCloudSecurityDB.Models.SecurityKey> secKey = AppDB.securityDB.SecurityKey.Where(c => c.StoreAdmin.UserName.Equals(item.UserName)).ToList();
                                    foreach (var item1 in secKey)
                                    {
                                        item1.IsUpdateAvailable = false;
                                    }
                                }
                            }
                            AppDB.securityDB.SaveChanges();
                            
                            
                        }

                        AppDB.Logger.Info("AppUpdate Updated Successfully");
                        clearAll();
                        ClientScript.RegisterStartupScript(GetType(), "notifier2", "successAlert('AppUpdate Updated Succesffully');", true);
                    }
                    else
                    {
                        AppDB.Logger.Info("Required fields should not be empty.");
                        ClientScript.RegisterStartupScript(GetType(), "notifier3", "errorAlert('Required fields should not be empty');", true);
                    }

                }
            }
        }
        protected void ddlappname_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlappname.SelectedItem.Text != "Select")
            {
                lock (lockTarget)
                {
                    TouchonCloudSecurityDB.Models.RegisteredApplications regApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text)).FirstOrDefault();
                    if (regApp.IsUpdateAvailable == true)
                    {
                        chkupdates.Checked = true;
                    }
                    else
                    {
                        chkupdates.Checked = false;
                    }
                }

            }
            else
            {
                AppDB.Logger.Info("Required fields should not be empty.");
                ClientScript.RegisterStartupScript(GetType(), "notifier4", "errorAlert('Required fields should not be empty');", true);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            clearAll();
            ClientScript.RegisterStartupScript(GetType(), "notifier5", "messageAlert('Data has been reset');", true);
        }

        private void clearAll()
        {
            ddlappname.ClearSelection();
            ddlappname.Items.FindByText("Select").Selected = true;
            chkupdates.Checked = false;
        }
    }
}