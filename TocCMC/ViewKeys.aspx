﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ViewKeys.aspx.cs" Inherits="TocCMC.ViewKeys" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <script type="text/javascript">
        function Validate() {

            var x = confirm("Are you sure you want to delete?");
            if (x == true)
                return true;
            else {
                if (document.getElementById("<%=Label1.ClientID%>") != null)
				         document.getElementById("<%=Label1.ClientID%>").innerText = "";
                     return false;
                 }

             }
    </script>
    <script type="text/javascript">
        function SelectSingleRadiobutton(rdbtnid) {
            var rdBtn = document.getElementById(rdbtnid);
            var rdBtnList = document.getElementsByTagName("input");
            for (i = 0; i < rdBtnList.length; i++) {
                if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                    rdBtnList[i].checked = false;
                }
            }
        }
    </script>
    <div class="container col-xs-12">
        <div id="mainContainer">
            <div id="tblPanel">
                <div class="col-md-2 col-sm-6">
                    <div class="list-group">
                        <div class="list-group-item">
                            <asp:Button ID="btnAddNew" runat="server" Text="New Key" OnClick="btnAddNew_Click" class="btn btn-primary" Style="width: 100%;" />
                            <asp:Label ID="Label1" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-sm-6">
                    <div class="table-responsive">
                        <asp:GridView ID="gdviewkey" runat="server" AutoGenerateColumns="False" OnSorting="gdviewkey_Sorting"
                            OnRowEditing="gdviewkey_RowEditing" AllowSorting="true" OnRowUpdating="gdviewkey_RowUpdating"
                            OnRowCancelingEdit="gdviewkey_RowCancelingEdit" OnRowCommand="gdviewkey_RowCommand"
                            class="table table-hover table-bordered table-striped">
                            <Columns>
                                <asp:TemplateField HeaderText="SNo" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblkey" runat="server" Text='<%#Eval("strSNo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ApplicationName" SortExpression="strAppName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblappname" runat="server" Text='<%#Eval("strAppName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblappname" runat="server" Enabled="false" Text='<%#Eval("strAppName") %>'></asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CMC Name" SortExpression="strConfig">
                                    <ItemTemplate>
                                        <asp:Label ID="lblcmcconfig" runat="server" Text='<%#Eval("strConfig") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Store Name" SortExpression="strStoreName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStoreName" runat="server" Text='<%#Eval("strStoreName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblStoreName" runat="server" Enabled="false" Text='<%#Eval("strStoreName") %>'></asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User Name" SortExpression="strUserName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserName" runat="server" Text='<%#Eval("strUserName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblUserName" runat="server" Enabled="false" Text='<%#Eval("strUserName") %>'></asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="System Name" SortExpression="strSysName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSysName" runat="server" Text='<%#Eval("strSysName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Authorization Code" SortExpression="strAuthCode">
                                    <ItemTemplate>
                                        <asp:Label ID="lblauthcode" runat="server" Text='<%#Eval("strAuthCode") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblauthcode" runat="server" Enabled="false" Text='<%#Eval("strAuthCode") %>'></asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Service Status" SortExpression="strIsActive">
                                    <ItemTemplate>
                                        <asp:Label ID="lblenable" runat="server" Text='<%#Eval("strIsActive") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlenable" runat="server" class="form-control" Font-Size="Smaller">
                                            <asp:ListItem>Enable</asp:ListItem>
                                            <asp:ListItem>Disable</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Update Status" SortExpression="strIsUpdate">
                                    <ItemTemplate>
                                        <asp:Label ID="lblupdate" runat="server" Text='<%#Eval("strIsUpdate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlupdate" runat="server"  class="form-control" Font-Size="Smaller">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:ListItem>No</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="LnkBtnUpdate" runat="server" CausesValidation="True"
                                            CommandName="Update" Text="Update" class="btn btn-primary"></asp:LinkButton>
                                        &nbsp;<asp:LinkButton ID="LnkBtnCancel" runat="server" CausesValidation="False"
                                            CommandName="Cancel" Text="Cancel" class="btn btn-default"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LnkBtnEdit" runat="server" CausesValidation="False"
                                            CommandName="Edit" Text="Edit" class="btn btn-primary"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="View">
                                    <ItemTemplate>
                                        <asp:Button ID="BtnView" runat="server" CommandName="Detail" Text="View" data-toggle="modal"
                                            data-target="#myModalViewKeys" class="btn btn-default" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="myModalViewKeys" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Key Generator</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    SNo
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lblid" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Customer Name
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lblcustname" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    System  Name
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lblsysname" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Customer Email
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lblcustemail" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Authorization Code
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lblauth" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    GUID
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lblguid" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    HDD Serial
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lblhddserial" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Device ID
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lbldeviceid" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    HDD Inter Type
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lblhddinter" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Mobile No
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lblmobile" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Confidential Text
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lblconftext" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Media Type
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lblmediatype" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnDelete" runat="server" class="btn btn-primary" Text="Delete" OnClientClick="return Validate();" OnClick="btnDelete_Click" />
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </div>
</asp:Content>
