﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KeyGeneratorPage.aspx.cs" Inherits="TocCMC.KeyGeneratorPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">
        function CheckGeneratedKey() {
            var txtGeneratedKey = document.getElementById('<%=txtGeneratedKey.ClientID %>');
            //txtGeneratedKey key logic
            if (txtGeneratedKey.value.length == 0) {
                warningAlert('Generated key should not be empty');
                $("#generatedKey").attr("class", "row form-group has-error");
                return false;
            }
            else {
                $("#generatedKey").attr("class", "row form-group");
                return true;
            }
        }

        function Reset() {
            var txtStoreName = document.getElementById('<%=txtStoreName.ClientID %>');
            var txtMobileNumber = document.getElementById('<%=txtMobileNumber.ClientID %>');
            var txtSystemName = document.getElementById('<%=txtSystemName.ClientID %>');
            var txthddserial = document.getElementById('<%=txthddserial.ClientID %>');
            var txtdeviceId = document.getElementById('<%=txtdeviceId.ClientID %>');
            var txtCustomerEmail = document.getElementById('<%=txtCustomerEmail.ClientID %>');
            var txtconfidential = document.getElementById('<%=txtconfidential.ClientID %>');
            var txtguid = document.getElementById('<%=txtguid.ClientID %>');
            var txthddintertype = document.getElementById('<%=txthddintertype.ClientID %>');
            var txtmediatype = document.getElementById('<%=txtmediatype.ClientID %>');
            var txtGeneratedKey = document.getElementById('<%=txtGeneratedKey.ClientID %>');
            txtStoreName.value = "";
            txtMobileNumber.value = "";
            txtSystemName.value = "";
            txthddserial.value = "";
            txtdeviceId.value = "";
            txtCustomerEmail.value = "";
            txtconfidential.value = "";
            txtguid.value = "";
            txthddintertype.value = "";
            txtmediatype.value = "";
            txtGeneratedKey.value = "";
            return true;
        }
        function Validate() {
            var ddlApp = document.getElementById('<%=ddlApp.ClientID %>');
            var ddlApp1 = $(ddlApp).find('option').filter(':selected').text();
            var ddlStoreAdmins = document.getElementById('<%=ddlStoreAdmins.ClientID %>');
            var ddlStoreAdmins1 = $(ddlStoreAdmins).find('option').filter(':selected').text();
            
            var txtStoreName = document.getElementById('<%=txtStoreName.ClientID %>'); 
            var txtMobileNumber = document.getElementById('<%=txtMobileNumber.ClientID %>');
            var txtSystemName = document.getElementById('<%=txtSystemName.ClientID %>');
            var txthddserial = document.getElementById('<%=txthddserial.ClientID %>');
            var txtdeviceId = document.getElementById('<%=txtdeviceId.ClientID %>');
            var txtCustomerEmail = document.getElementById('<%=txtCustomerEmail.ClientID %>');
            var txtconfidential = document.getElementById('<%=txtconfidential.ClientID %>');
            var txtguid = document.getElementById('<%=txtguid.ClientID %>');
            var txthddintertype = document.getElementById('<%=txthddintertype.ClientID %>');
            var txtmediatype = document.getElementById('<%=txtmediatype.ClientID %>'); 
            
            //ddl app logic
            if (ddlApp1 == "Select" || ddlApp1 == "") {
                warningAlert('Select Application');
                $("#selectApp").attr("class", "row form-group has-error");
            }
            else {
                $("#selectApp").attr("class", "row form-group");
            }
            //ddlStoreAdmins logic
            if (ddlStoreAdmins1 == "Select" || ddlStoreAdmins1 == "") {
                warningAlert('Select User');
                $("#selectUser").attr("class", "row form-group has-error");
            }
            else {
                $("#selectUser").attr("class", "row form-group");
            }

            //txtStoreName key logic
            if (txtStoreName.value.length == 0) {
                warningAlert('Store Name should not be empty');
                $("#storeName").attr("class", "row form-group has-error");
            }
            else {
                $("#storeName").attr("class", "row form-group");
            }
            //txtMobileNumber key logic
            if (txtMobileNumber.value.length == 0) {
                warningAlert('Mobile Number should not be empty');
                $("#mobNumber").attr("class", "row form-group has-error");
            }
            else {
                $("#mobNumber").attr("class", "row form-group");
            }            
            //txtSystemName key logic
            if (txtSystemName.value.length == 0) {
                warningAlert('Enter System name');
                $("#sysName").attr("class", "row form-group has-error");
            }
            else {
                $("#sysName").attr("class", "row form-group");
            }
            //txthddserial key logic
            if (txthddserial.value.length == 0) {
                warningAlert('Enter HDD Serial key');
                $("#hddSerial").attr("class", "row form-group has-error");
            }
            else {
                $("#hddSerial").attr("class", "row form-group");
            }
            //txtdeviceId key logic
            if (txtdeviceId.value.length == 0) {
                warningAlert('Enter Device ID');
                $("#deviceID").attr("class", "row form-group has-error");
            }
            else {
                $("#deviceID").attr("class", "row form-group");
            }
            //txtCustomerEmail key logic
            if (txtCustomerEmail.value.length == 0) {
                warningAlert('Store Email should not be empty');
                $("#storeEmail").attr("class", "row form-group has-error");
            }
            else {
                $("#storeEmail").attr("class", "row form-group");
            }
            //txtCustomerEmail key logic
            if (txtconfidential.value.length == 0) {
                warningAlert('Confidential should not be empty');
                $("#confidentialText").attr("class", "row form-group has-error");
            }
            else {
                $("#confidentialText").attr("class", "row form-group");
            }
            //txtguid key logic
            if (txtguid.value.length == 0) {
                warningAlert('Enter GUID');
                $("#guid").attr("class", "row form-group has-error");
            }
            else {
                $("#guid").attr("class", "row form-group");
            }
            //txthddintertype key logic
            if (txthddintertype.value.length == 0) {
                warningAlert('Enter HDD Inter Type');
                $("#hddInterType").attr("class", "row form-group has-error");
            }
            else {
                $("#hddInterType").attr("class", "row form-group");
            }
            //txtmediatype key logic
            if (txtmediatype.value.length == 0) {
                warningAlert('Enter Media Type');
                $("#mediaType").attr("class", "row form-group has-error");
            }
            else {
                $("#mediaType").attr("class", "row form-group");
            }
            
            if (ddlApp1 != "Select" && ddlApp1 != "" && txtStoreName.value.length != 0 && txtMobileNumber.value.length != 0
                && txtSystemName.value.length != 0 && txthddserial.value.length != 0 && txtdeviceId.value.length != 0
                && ddlStoreAdmins1 != "Select" && ddlStoreAdmins1 != "" && txtCustomerEmail.value.length != 0
                && txtconfidential.value.length != 0 && txtguid.value.length != 0 && txthddintertype.value.length != 0
                && txtmediatype.value.length != 0) {

                return true;
            }
            else {
                errorAlert('Enter Required Fields');
                return false;
            }
        }
    </script>

    <div class="col-xs-10 col-centered" style="margin-top: 3%;">
        <div class="panel panel-info">
            <div class="panel-heading">Key Generator</div>
            <div class="panel-body">
                <div class="col-md-6 col-sm-6">

                    <div class="row form-group" id="selectApp">
                        <div class="col-md-4 col-sm-6">
                            Select Application
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <asp:DropDownList ID="ddlApp" runat="server" AppendDataBoundItems="true" class="form-control" AutoPostBack="true"
                                DataTextField="AppName" DataValueField="SNo" OnSelectedIndexChanged="ddlApp_SelectedIndexChanged">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="row form-group" id="storeName">
                        <div class="col-md-4 col-sm-6">
                            Store Name
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <asp:TextBox ID="txtStoreName" runat="server" class="form-control" ReadOnly="true"/>
                        </div>
                    </div>

                    <div class="row form-group" id="mobNumber">
                        <div class="col-md-4 col-sm-6">
                            Mobile Number
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <asp:TextBox ID="txtMobileNumber" runat="server" class="form-control" ReadOnly="true"/>
                            <asp:RegularExpressionValidator ID="rev1" runat="server" ControlToValidate="txtMobileNumber" Display="Dynamic" Font-Size="Smaller" ValidationExpression="[0-9]{10}" ForeColor="Red" ErrorMessage="Accepts only Numerics"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="row form-group" id="sysName">
                        <div class="col-md-4 col-sm-6">
                            System Name
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <asp:TextBox ID="txtSystemName" runat="server" class="form-control" />
                        </div>
                    </div>

                    <div class="row form-group" id="hddSerial">
                        <div class="col-md-4 col-sm-6">
                            HDD Serial
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <asp:TextBox ID="txthddserial" runat="server" class="form-control" />
                        </div>
                    </div>
                    <div class="row form-group" id="deviceID">
                        <div class="col-md-4 col-sm-6">Device ID</div>
                        <div class="col-md-6 col-sm-6">
                            <asp:TextBox ID="txtdeviceId" runat="server" class="form-control" /></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4 col-sm-6"></div>
                        <div class="col-md-6 col-sm-6">
                            <asp:Button ID="btnGenerate" runat="server" Text="Generate" class="btn btn-primary"
                               OnClientClick="javascript:return Validate()"
                                 OnClick="btnGenerate_Click" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">

                    <div class="row form-group" id="selectUser">
                        <div class="col-md-4 col-sm-6">Select User</div>
                        <div class="col-md-6 col-sm-6">
                            <asp:DropDownList ID="ddlStoreAdmins" runat="server" AppendDataBoundItems="true" class="form-control"
                                AutoPostBack="true" DataTextField="UserName" DataValueField="SNo" OnSelectedIndexChanged="ddlStoreAdmins_SelectedIndexChanged">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="row form-group" id="storeEmail">
                        <div class="col-md-4 col-sm-6">Store Email</div>
                        <div class="col-md-6 col-sm-6">
                            <asp:TextBox ID="txtCustomerEmail" runat="server"  ReadOnly="true" class="form-control" /></div>
                    </div>

                    <div class="row form-group" id="confidentialText">
                        <div class="col-md-4 col-sm-6">Confidential Text</div>
                        <div class="col-md-6 col-sm-6">
                            <asp:TextBox ID="txtconfidential" runat="server" ReadOnly="true" class="form-control" /></div>
                    </div>

                    <div class="row form-group" id="guid">
                        <div class="col-md-4 col-sm-6">Enter GUID</div>
                        <div class="col-md-6 col-sm-6">
                            <asp:TextBox ID="txtguid" runat="server" class="form-control" /></div>
                    </div>

                    <div class="row form-group" id="hddInterType">
                        <div class="col-md-4 col-sm-6">HDD Inter Type</div>
                        <div class="col-md-6 col-sm-6">
                            <asp:TextBox ID="txthddintertype" runat="server" class="form-control" /></div>
                    </div>

                    <div class="row form-group" id="mediaType">
                        <div class="col-md-4 col-sm-6">Media Type</div>
                        <div class="col-md-6 col-sm-6">
                            <asp:TextBox ID="txtmediatype" runat="server" class="form-control" /></div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-4 col-sm-6"></div>
                        <div class="col-md-6 col-sm-6">
                            <asp:CheckBox ID="chkEnabled" runat="server" Checked="true" Visible="false" Text="Is Enabled" /></div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-4 col-sm-6"></div>
                        <div class="col-md-6 col-sm-6">
                            <asp:CheckBox ID="chkavail" runat="server" Checked="true" Visible="false" Text="Is Update Available" /></div>
                    </div>

                    <div class="row form-group" id="generatedKey">
                        <div class="col-md-4 col-sm-6">Generated Key</div>
                        <div class="col-md-6 col-sm-6">
                            <asp:TextBox ID="txtGeneratedKey" runat="server" ReadOnly="true" CssClass="form-control" /></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4 col-sm-6"></div>
                        <div class="col-md-8 col-sm-8">
                            <asp:Button ID="btnSendMail" runat="server" Text="Save & Send Mail" class="btn btn-primary"
                                OnClientClick="javascript:return CheckGeneratedKey()"
                                OnClick="btnSendMail_Click" />
                            <asp:Button ID="btnReset" runat="server" Text="Reset" class="btn btn-default"
                            OnClientClick="javascript:return Reset()"
                            OnClick="btnReset_Click" />
                        <asp:Button ID="btnBack" runat="server" Text="Back" class="btn btn-default"
                            OnClientClick="javascript:return Reset()"
                            OnClick="btnBack_Click" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
