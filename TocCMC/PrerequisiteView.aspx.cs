﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TocCMC
{
    public partial class PrerequisiteView : System.Web.UI.Page
    {
        object lockTarget = new object();
        DataTable dt;
        DataRow dr;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    lock (lockTarget)
                    {
                        AppDB.Logger.Info("Initiating View Pre-Requisite.");
                        //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                        AppDB.Logger.Info("Checking for Atleast One Pre-Requisite exists in DB..");
                        if (AppDB.securityDB.RegisterPrerequisites.ToList().Count > 0)
                        {
                            GetTotalData();
                        }
                    }
                }
            }
        }
        private void GetTotalData()
        {
            lock (lockTarget)
            {
                dt = new DataTable();
                dr = null;
                dt.Columns.Add(new DataColumn("strSno", typeof(long)));
                dt.Columns.Add(new DataColumn("strName", typeof(string)));
                dt.Columns.Add(new DataColumn("strFtpUserName", typeof(string)));
                dt.Columns.Add(new DataColumn("strUpdatedTime", typeof(string)));
                foreach (var item in AppDB.securityDB.RegisterPrerequisites.ToList())
                {
                    long strSno = item.SNo;
                    TouchonCloudSecurityDB.Models.RegisterPrerequisite regprerequisite = AppDB.securityDB.RegisterPrerequisites.Where(c => c.SNo.Equals(strSno)).FirstOrDefault();
                    if (regprerequisite != null)
                    {
                        string strName = regprerequisite.Name;
                        string strFtpUserName = regprerequisite.FtpUserName;
                        string strUpdatedTime = regprerequisite.UpdatedTime.ToString();
                        dr = dt.NewRow();
                        dr["strSno"] = strSno;
                        dr["strName"] = strName;
                        dr["strFtpUserName"] = strFtpUserName;
                        dr["strUpdatedTime"] = strUpdatedTime;
                        dt.Rows.Add(dr);
                        ViewState["CurrentTable"] = dt;
                    }
                }
                AppDB.Logger.Info("Fetching all the pre-requisite  from DB to the GridView");
                gdviewrequisite.DataSource = dt;
                gdviewrequisite.DataBind();
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            AppDB.Logger.Info("Initiating pre-requisite ID: " + lblid.Text + " pre-requisite Name: " + txtName.Text);
            if (!string.IsNullOrEmpty(txtName.Text) && !string.IsNullOrEmpty(txtftppwd.Text) && !string.IsNullOrEmpty(txtftppath.Text) && !string.IsNullOrEmpty(txtftpusername.Text))
            {
                lock (lockTarget)
                {
                    AppDB.Logger.Info("Application Config validating completed.");
                    long id = Convert.ToInt64(lblid.Text);
                    TouchonCloudSecurityDB.Models.RegisterPrerequisite updateprerequisite = AppDB.securityDB.RegisterPrerequisites.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                    if (updateprerequisite != null)
                    {

                        if (AppDB.securityDB.RegisterPrerequisites.ToList().Exists(c => c.Name.Equals(txtName.Text, StringComparison.OrdinalIgnoreCase)))
                        {
                            try
                            {
                                updateprerequisite.Name = txtName.Text.Trim();
                                updateprerequisite.Description = txtDesc.Text;
                                updateprerequisite.FtpUserName = txtftpusername.Text.Trim();
                                updateprerequisite.FtpPath = txtftppath.Text.Trim();
                                updateprerequisite.FtpPassword = txtftppwd.Text.Trim();
                                if (radInstallerMode.SelectedItem.Text.Equals("Automatic"))
                                {
                                    updateprerequisite.InstallerMode = TouchonCloudSecurityDB.Enums.InstallerMode.Automatic;
                                }
                                else
                                {
                                    updateprerequisite.InstallerMode = TouchonCloudSecurityDB.Enums.InstallerMode.Manual;
                                }
                                updateprerequisite.LaunchPath = txtLaunchPath.Text.Trim();
                                if (radExtraction.SelectedItem.Text.Equals("Enable"))
                                {
                                    updateprerequisite.ExtractEnabled = "Y";
                                }
                                else
                                {
                                    updateprerequisite.ExtractEnabled = "N";
                                }
                                if (radList.SelectedItem.Text.Equals("Enable"))
                                {
                                    updateprerequisite.IsActive = true;
                                }
                                else
                                {
                                    updateprerequisite.IsActive = false;
                                }
                                updateprerequisite.UpdatedTime = DateTime.Now;
                                AppDB.securityDB.SaveChanges();
                                GetTotalData();
                                ClientScript.RegisterStartupScript(GetType(), "notifier6", "successAlert('Pre-Requisite Updated Successfully');", true);


                            }
                            catch (Exception ex)
                            {
                                AppDB.Logger.ErrorException(ex.Message, ex);
                                ClientScript.RegisterStartupScript(GetType(), "notifier7", "errorAlert('" + ex.Message + "');", true);
                            }
                        }


                    }
                    else
                    {
                        AppDB.Logger.Info("Configuration id not found.");
                        ClientScript.RegisterStartupScript(GetType(), "notifier9", "errorAlert('Pre-Requisite id not found');", true);
                    }
                }
            }
            else
            {
                AppDB.Logger.Info("Invalid Configuration.");
                ClientScript.RegisterStartupScript(GetType(), "notifier10", "errorAlert('Invalid Pre-requisite');", true);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            lock (lockTarget)
            {
                GetSelectedItemData();
                System.Text.StringBuilder sbj = new System.Text.StringBuilder();
                sbj.Append("<script type='text/javascript'>");
                sbj.Append("$('#myModalViewPrerequisite').modal('show');");
                sbj.Append("</script>");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", sbj.ToString(), false);
            }
        }
        private void GetSelectedItemData()
        {
            long id = Convert.ToInt64(lblid.Text);
            TouchonCloudSecurityDB.Models.RegisterPrerequisite prerequisite = AppDB.securityDB.RegisterPrerequisites.Where(c => c.SNo.Equals(id)).FirstOrDefault();
            txtDesc.Text = prerequisite.Description;
            txtName.Text = prerequisite.Name;
            txtftppath.Text = prerequisite.FtpPath;
            txtftppwd.Text = prerequisite.FtpPassword;
            txtftpusername.Text = prerequisite.FtpUserName;
            txtLaunchPath.Text = prerequisite.LaunchPath;
            if (prerequisite.InstallerMode.Equals(TouchonCloudSecurityDB.Enums.InstallerMode.Automatic))
            {
                radInstallerMode.Items.FindByText("Automatic").Selected = true;
            }
            else
            {
                radInstallerMode.Items.FindByText("Manual").Selected = true;
            }
            if (prerequisite.ExtractEnabled.Equals("Y"))
            {
                radExtraction.Items.FindByText("Enable").Selected = true;
            }
            else
            {
                radExtraction.Items.FindByText("Disable").Selected = true;
            }
            if (prerequisite.IsActive)
            {
                radList.Items.FindByText("Enable").Selected = true;
            }
            else
            {
                radList.Items.FindByText("Disable").Selected = true;
            }
        }

        protected void btnPreRequisite_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewPreRequisite.aspx");
        }
        protected void gdviewrequisite_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dataTable = ViewState["CurrentTable"] as DataTable;
            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression;
                gdviewrequisite.DataSource = dataView;
                gdviewrequisite.DataBind();
            }
        }
        protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            int Index;
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            for (int i = 0; i < gdviewrequisite.Rows.Count; i++)
            {
                GridViewRow row = gdviewrequisite.Rows[i];
                RadioButton rdb = (RadioButton)row.FindControl("RadioButton1");
                if (rdb.Checked == true)
                {
                    lock (lockTarget)
                    {
                        Index = row.RowIndex;
                        lblid.Text = dt.Rows[Index]["strSno"].ToString();
                        long id = Convert.ToInt64(lblid.Text);
                        TouchonCloudSecurityDB.Models.RegisterPrerequisite prerequisite = AppDB.securityDB.RegisterPrerequisites.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                        txtDesc.Text = prerequisite.Description;
                        txtName.Text = prerequisite.Name;
                        txtftppath.Text = prerequisite.FtpPath;
                        txtftppwd.Text = prerequisite.FtpPassword;
                        txtftpusername.Text = prerequisite.FtpUserName;
                        txtLaunchPath.Text = prerequisite.LaunchPath;
                        if(prerequisite.InstallerMode.Equals(TouchonCloudSecurityDB.Enums.InstallerMode.Automatic))
                        {
                            radInstallerMode.Items.FindByText("Automatic").Selected = true;
                        }
                        else
                        {
                            radInstallerMode.Items.FindByText("Manual").Selected = true;
                        }
                        if(prerequisite.ExtractEnabled.Equals("Y"))
                        {
                            radExtraction.Items.FindByText("Enable").Selected = true;
                        }
                        else
                        {
                            radExtraction.Items.FindByText("Disable").Selected = true;
                        }
                        if(prerequisite.IsActive)
                        {
                            radList.Items.FindByText("Enable").Selected = true;
                        }
                        else
                        {
                            radList.Items.FindByText("Disable").Selected = true;
                        }
                    }
                }
            }
        }
        protected void btnEditPrerequisite_Click(object sender, EventArgs e)
        {
            bool isChecked = false;
            foreach (GridViewRow row in gdviewrequisite.Rows)
            {
                RadioButton rb = (RadioButton)row.FindControl("RadioButton1");
                if (rb.Checked)
                {
                    isChecked = true;
                }
            }

            if (!isChecked)
            {
                ClientScript.RegisterStartupScript(GetType(), "notifier1", "warningAlert('Please Select Anyone');", true);
            }
            else
            {
                System.Text.StringBuilder sbj = new System.Text.StringBuilder();
                sbj.Append("<script type='text/javascript'>");
                sbj.Append("$('#myModalViewPrerequisite').modal('show');");
                sbj.Append("</script>");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", sbj.ToString(), false);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            long id = 0;
            int Index;
            DataTable dt = (DataTable)ViewState["CurrentTable"];

            for (int i = 0; i < gdviewrequisite.Rows.Count; i++)
            {
                GridViewRow row = gdviewrequisite.Rows[i];
                RadioButton rb = (RadioButton)row.FindControl("RadioButton1");
                if (rb.Checked)
                {
                    Index = row.RowIndex;
                    //int strI = dt.Rows.IndexOf(dt.Rows[Index]);
                    string strId = dt.Rows[Index]["strSno"].ToString();
                    try
                    {
                        id = Convert.ToInt64(strId);
                        AppDB.Logger.Info("Initiating Delete Pre-Requisite ID: " + id);
                        TouchonCloudSecurityDB.Models.RegisterPrerequisite updatePrerequisite = AppDB.securityDB.RegisterPrerequisites.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                        if (updatePrerequisite != null)
                        {
                            AppDB.securityDB.RegisterPrerequisites.Remove(updatePrerequisite);
                            AppDB.securityDB.SaveChanges();
                            ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('Pre-Requisite Deleted Successfully');", true);
                            GetTotalData();
                        }
                    }
                    catch (Exception ex)
                    {
                        AppDB.Logger.ErrorException(ex.Message, ex);
                        ClientScript.RegisterStartupScript(GetType(), "notifier5", "errorAlert('" + ex.Message + "');", true);
                    }
                }
            }
        }
    }
}