﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TocCMC
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtusername.Focus();
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtusername.Text != null && txtpwd.Text != null)
                {
                    if (txtusername.Text == "admin" && txtpwd.Text == "admin1")
                    {
                        Session["username"] = txtusername.Text;
                        Response.Redirect("Home.aspx");
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "notifier5", "errorAlert('please enter valid username and password');", true);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "notifier6", "errorAlert('please enter required fields');", true);
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtusername.Text = "";
            txtpwd.Text = "";
        }
    }
}