﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="CreateCnfKey.aspx.cs" Inherits="TocCMC.CreateCnfKey" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
     <script type="text/javascript">
         function Reset() {
             $(".ddlApp").get(0).selectedIndex = 0;
             $(".secretkey").val("");
            return true;
        }
         $(document).ready(function () {
             $(".ddlApp").change(function () {
                 var ddlApp = $(".ddlApp").val();
                 if (ddlApp == "Select") {
                     $(".ddlAppErr").text("please select application");
                     return false;
                 }
                 else {
                     $(".ddlAppErr").text("");
                 }
             });
             $(".secretkey").keyup(function () {
                 var secretkey = $(".secretkey").val();    //gets the value of class secretkey
                 if (secretkey == "")     //secretkey should not be empty
                 {
                     $(".secretkeyErr").text("please enter secret key");
                     return false;
                 }
                 if (secretkey.length < 4)    //secretkey minimum length is 4 characters
                 {
                     $(".secretkeyErr").text("minimum 4 characters");
                     return false;
                 }
                 if (secretkey.length > 30)   //secretkey maximum length is 30 characters
                 {
                     $(".secretkeyErr").text("maximum 30 characters");
                     return false;
                 }
                 else {
                     $(".secretkeyErr").text("");
                 }
             });
             $(".validate").click(function () {
                 var count = 0;
                 var secretkey = $(".secretkey").val();
                 var ddlApp = $(".ddlApp").val();
                 if (ddlApp == "Select") {
                     $(".ddlAppErr").text("please select application");
                     count = count + 1;
                 }
                 if (secretkey.length < 4)    //secretkey minimum length is 4 characters
                 {
                     $(".secretkeyErr").text("minimum 4 characters");
                     count = count + 1;
                 }
                 if (secretkey.length > 30)   //secretkey maximum length is 30 characters
                 {
                     $(".secretkeyErr").text("maximum 30 characters");
                     count = count + 1;
                 }
                 if (secretkey == "")     //secretkey should not be empty
                 {
                     $(".secretkeyErr").text("please enter secret key");
                     count = count + 1;
                 }

                 if (count > 0) {
                     return false;
                 }
             });
         });
    </script>
    <div class="col-md-6 col-sm-5 col-centered" style="margin-top: 4%;">
        <div class="panel panel-info">
            <div class="panel-heading">Create Confidential Key</div>
            <div class="panel-body">
                <div class="row form-group" id="appName">
                    <div class="col-md-3 col-sm-3">Select App Name</div>
                    <div class="col-md-6 col-sm-6">
                        <asp:DropDownList ID="ddlappname" runat="server" DataTextField="AppName" DataValueField="SNo"
                            AppendDataBoundItems="true" class="form-control ddlApp">
                            <asp:ListItem Text="Select"></asp:ListItem>
                        </asp:DropDownList>
                        <span class="ddlAppErr error-code"></span>
                    </div>
                </div>
                <div class="row form-group" id="secretKey">
                    <div class="col-md-3 col-sm-3">Secret Key</div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtsecretkey" runat="server" class="form-control secretkey" />
                        <span class="secretkeyErr error-code"></span>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 col-sm-3"></div>
                    <div class="col-md-6 col-sm-6">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-primary validate" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnReset" runat="server" Text="Reset" class="btn btn-default"
                            OnClientClick="javascript:return Reset()"
                            OnClick="btnReset_Click" />
                        <asp:Button ID="btnBack" runat="server" Text="Back" class="btn btn-default"
                            OnClientClick="javascript:return Reset()"
                            OnClick="btnBack_Click" />
                    </div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
