﻿using TouchonCloudSecurityDB.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TocCMC
{
    public partial class ViewApps : System.Web.UI.Page
    {
        object lockTarget = new object();
        string strUpdateAvail, strIsActive;
        DataTable dt, dtSearch;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    lock (lockTarget)
                    {
                        AppDB.Logger.Info("Initiating View Apps.");
                        //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                        AppDB.Logger.Info("Checking for Atleast One Application Name exists in DB..");
                        if (AppDB.securityDB.RegApps.ToList().Count > 0)
                        {
                            GetTotalData();
                            ddlappname.DataSource = dt;
                            ddlappname.DataBind();
                        }
                    }
                }
            }
        }
        private void GetTotalData()
        {
            lock (lockTarget)
            {
                //dt.Clear();
                dt = new DataTable();
                DataRow dr = null;
                dt.Columns.Add(new DataColumn("strSno", typeof(long)));
                dt.Columns.Add(new DataColumn("strAppName", typeof(string)));
                dt.Columns.Add(new DataColumn("strVerison", typeof(string)));
                dt.Columns.Add(new DataColumn("strUpdatedTime", typeof(string)));
                dt.Columns.Add(new DataColumn("strUpdateAvail", typeof(string)));
                dt.Columns.Add(new DataColumn("strIsActive", typeof(string)));
                foreach (var item in AppDB.securityDB.RegApps.ToList())
                {
                    string strSno = item.AppName;
                    lock (lockTarget)
                    {
                       
                        TouchonCloudSecurityDB.Models.RegisteredApplications regApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(strSno)).FirstOrDefault();
                        if (regApp != null)
                        {
                            long id = regApp.SNo;
                            string strAppName = regApp.AppName;
                            string strVerison = regApp.Version;
                            string strUpdatedTime = regApp.UpdatedTime.ToString();
                            if (regApp.IsUpdateAvailable)
                            {
                                strUpdateAvail = "Enable";
                            }
                            else
                            {
                                strUpdateAvail = "Disable";
                            }
                            if (regApp.IsActive)
                            {
                                strIsActive = "Active";
                            }
                            else
                            {
                                strIsActive = "Inactive";
                            }
                            dr = dt.NewRow();

                            dr["strSno"] = id;
                            dr["strAppName"] = strAppName;
                            dr["strVerison"] = strVerison;
                            dr["strUpdatedTime"] = strUpdatedTime;
                            dr["strUpdateAvail"] = strUpdateAvail;
                            dr["strIsActive"] = strIsActive; ;
                            dt.Rows.Add(dr);
                            //dr = dt.NewRow();

                            //Store the DataTable in ViewState
                            ViewState["CurrentTable"] = dt;
                        }
                    }

                }
                AppDB.Logger.Info("Fetchning all the Applications from Datatable to the Gridview");

                gdviewapp.DataSource = dt;
                gdviewapp.DataBind();
            }
        }
        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("RegisterApp.aspx");
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            AppDB.Logger.Info("Initiating Updating  Application Name: " + txtappname.Text + " Started.");

            AppDB.Logger.Info("Checking for validations.");
            string strId1 = txtappname.Text;
            lock (lockTarget)
            {
                RegisteredApplications regApplication = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(strId1)).FirstOrDefault();
                if (regApplication != null && !string.IsNullOrEmpty(txtappname.Text))
                {
                    if (txtappname.Text.Equals(regApplication.AppName, StringComparison.OrdinalIgnoreCase))
                    {
                        AppDB.Logger.Info("Application Valid.");
                        try
                        {
                            regApplication.AppName = txtappname.Text.Trim();
                            regApplication.UpdatedTime = DateTime.Now;
                            regApplication.Version = txtversion.Text.Trim('.');
                            AppDB.securityDB.SaveChanges();
                            GetTotalData();
                            ClientScript.RegisterStartupScript(GetType(), "notifier1", "successAlert('Application Updated Successfully');", true);

                            
                            
                        }
                        catch (Exception ex)
                        {
                            AppDB.Logger.ErrorException(ex.Message, ex);
                            ClientScript.RegisterStartupScript(GetType(), "notifier2", "errorAlert('" + ex.Message + "');", true);
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "notifier3", "warningAlert('Application Name must not change');", true);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "notifier4", "errorAlert('Application ID doesn't exist');", true);
                }
            }
        }

        protected void gdviewapp_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Label1.Visible = false;
            int Index;
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            for (int i = 0; i < gdviewapp.Rows.Count; i++)
            {
                GridViewRow row = gdviewapp.Rows[i];
                RadioButton rb = (RadioButton)row.FindControl("RadioButton1");
                if (rb.Checked)
                {
                    Index = row.RowIndex;
                    string strId = dt.Rows[Index]["strAppName"].ToString();

                    AppDB.Logger.Info("Initiating Deleting Application Name: " + strId + " Started.");
                    if (strId != null)
                    {
                        lock (lockTarget)
                        {
                            AppDB.Logger.Info("Checking whether application exists or not.");
                            RegisteredApplications regApplication = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(strId)).FirstOrDefault();

                            if (regApplication != null)
                            {
                                if (regApplication.LstStoreAdmins == null && regApplication.LstCMCConfig == null && regApplication.ConfidentialKey == null)
                                {

                                    AppDB.Logger.Info("Application exists. Deleting Started.");
                                    try
                                    {
                                        lock (lockTarget)
                                        {
                                            AppDB.securityDB.RegApps.Remove(regApplication);
                                            AppDB.securityDB.SaveChanges();
                                            GetTotalData();
                                            ClientScript.RegisterStartupScript(GetType(), "notifier5", "successAlert('Application Deleted Successfully');", true);
                                            
                                            
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        AppDB.Logger.ErrorException(ex.Message, ex);
                                        ClientScript.RegisterStartupScript(GetType(), "notifier6", "errorAlert('" + ex.Message + "');", true);
                                    }
                                }
                                else if (regApplication.LstStoreAdmins.Count == 0 && regApplication.LstCMCConfig.Count == 0 && regApplication.ConfidentialKey == null)
                                {

                                    AppDB.Logger.Info("Application exists. Deleting Started.");
                                    try
                                    {
                                        lock (lockTarget)
                                        {
                                            AppDB.securityDB.RegApps.Remove(regApplication);
                                            AppDB.securityDB.SaveChanges();
                                            GetTotalData();
                                            ClientScript.RegisterStartupScript(GetType(), "notifier7", "successAlert('Application Deleted Successfully');", true);
                                            
                                            
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        AppDB.Logger.ErrorException(ex.Message, ex);
                                        ClientScript.RegisterStartupScript(GetType(), "notifier8", "errorAlert('" + ex.Message + "');", true);
                                    }
                                }
                                else
                                {
                                    ClientScript.RegisterStartupScript(GetType(), "notifier9", "warningAlert('Please First Delete CMC Configuration || Users || Confidential Key and Try Again');", true);
                                }
                            }
                            else
                                ClientScript.RegisterStartupScript(GetType(), "notifier10", "errorAlert('Application ID doesn't exist');", true);
                        }
                    }
                    else
                        ClientScript.RegisterStartupScript(GetType(), "notifier11", "errorAlert('Invalid ID');", true);
                }
            }
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Label1.Visible = false;
            bool isChecked = false;
            foreach (GridViewRow row in gdviewapp.Rows)
            {
                RadioButton rb = (RadioButton)row.FindControl("RadioButton1");
                if (rb.Checked)
                {
                    isChecked = true;
                }
            }
            if (!isChecked)
            {
                ClientScript.RegisterStartupScript(GetType(), "notifier12", "warningAlert('Please Select Anyone');", true);
            }
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<script type='text/javascript'>");
                sb.Append("$('#myModal').modal('show');");
                sb.Append("</script>");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", sb.ToString(), false);
            }
        }
        protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            GetSelectedItemData();
        }

        private void GetSelectedItemData()
        {
            int Index;
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            for (int i = 0; i < gdviewapp.Rows.Count; i++)
            {
                GridViewRow row = gdviewapp.Rows[i];
                RadioButton rdb = (RadioButton)row.FindControl("RadioButton1");
                if (rdb.Checked == true)
                {
                    Index = row.RowIndex;
                    txtappname.Text = dt.Rows[Index]["strAppName"].ToString();
                    lock (lockTarget)
                    {
                        TouchonCloudSecurityDB.Models.RegisteredApplications regApp = AppDB.securityDB.RegApps.ToList().Where(c => c.AppName.Equals(txtappname.Text)).FirstOrDefault();
                        txtappname.Text = regApp.AppName;
                        txtupdatedtime.Text = regApp.UpdatedTime.ToString();
                        txtversion.Text = regApp.Version;
                    }
                }
            }
        }
        protected void gdviewapp_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dataTable = ViewState["CurrentTable"] as DataTable;
            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression;
                gdviewapp.DataSource = dataView;
                gdviewapp.DataBind();
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
        }
        protected void ddlappname_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlappname.SelectedItem.Text != "Select")
            {
                lock (lockTarget)
                {
                    gdviewapp.DataSource = null;
                    dtSearch = new DataTable();
                    DataRow dr1 = null;
                    dtSearch.Columns.Add(new DataColumn("strSno", typeof(long)));
                    dtSearch.Columns.Add(new DataColumn("strAppName", typeof(string)));
                    dtSearch.Columns.Add(new DataColumn("strVerison", typeof(string)));
                    dtSearch.Columns.Add(new DataColumn("strUpdatedTime", typeof(string)));
                    dtSearch.Columns.Add(new DataColumn("strUpdateAvail", typeof(string)));
                    dtSearch.Columns.Add(new DataColumn("strIsActive", typeof(string)));

                    {
                        TouchonCloudSecurityDB.Models.RegisteredApplications regApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text)).FirstOrDefault();
                        if (regApp != null)
                        {
                            long id = regApp.SNo;
                            string strAppName = regApp.AppName;
                            string strVerison = regApp.Version;
                            string strUpdatedTime = regApp.UpdatedTime.ToString();
                            if (regApp.IsUpdateAvailable)
                            {
                                strUpdateAvail = "Enable";
                            }
                            else
                            {
                                strUpdateAvail = "Disable";
                            }
                            if (regApp.IsActive)
                            {
                                strIsActive = "Active";
                            }
                            else
                            {
                                strIsActive = "Inactive";
                            }
                            dr1 = dtSearch.NewRow();

                            dr1["strSno"] = id;
                            dr1["strAppName"] = strAppName;
                            dr1["strVerison"] = strVerison;
                            dr1["strUpdatedTime"] = strUpdatedTime;
                            dr1["strUpdateAvail"] = strUpdateAvail;
                            dr1["strIsActive"] = strIsActive; ;
                            dtSearch.Rows.Add(dr1);
                            //dr = dt.NewRow();

                            //Store the DataTable in ViewState
                            ViewState["CurrentTable"] = dtSearch;
                        }

                    }
                    AppDB.Logger.Info("Fetchning all the Applications from Datatable to the Gridview");
                    gdviewapp.DataSource = dtSearch;
                    gdviewapp.DataBind();
                }
            }
            else
            {
                GetTotalData();
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            GetSelectedItemData();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type='text/javascript'>");
            sb.Append("$('#myModal').modal('show');");
            sb.Append("</script>");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", sb.ToString(), false);
        }
    }
}