﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="NewPreRequisite.aspx.cs" Inherits="TocCMC.NewPreRequisite" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function Reset() {
            $(".ftppath").val("ftp://");
            $(".username").val("");
            $(".password").val("");
            $(".locpath").val("");
            return true;
        }
        $(document).ready(function () {
           
            $(".ftppath").keyup(function () {
                var ftppath = $(".ftppath").val();    //gets the value of class ftppath            
                var ftpstartstring = ftppath.substring(0, 6);
                if (ftppath == "" || ftppath == "ftp://")     //ftppath should not be empty
                {
                    $(".ftppathErr").text("please enter ftp path");
                    $(".ftppath").val("ftp://");
                    return false;
                }
                if (ftpstartstring != "ftp://") {
                    $(".ftppathErr").text("please correct the path. 'eg: ftp://'");
                    $(".ftppath").val("ftp://");
                    return false;
                }
                else {
                    $(".ftppathErr").text("");
                }
            });
            $(".username").keyup(function () {
                var username = $(".username").val();    //gets the value of class username
                if (username == "")     //username should not be empty
                {
                    $(".usernameErr").text("please enter username");
                    return false;
                }
                if (username.length > 40)   //username maximum length is 40 characters
                {
                    $(".usernameErr").text("maximum 40 characters");
                    return false;
                }
                else {
                    $(".usernameErr").text("");
                }
            });
            $(".name").keyup(function () {
                var name = $(".name").val();    //gets the value of class username
                if (name == "")     //username should not be empty
                {
                    $(".nameErr").text("please enter pre-requisite name");
                    return false;
                }
                else {
                    $(".nameErr").text("");
                }
            });
            $(".password").keyup(function () {
                var password = $(".password").val();    //gets the value of class password                
                if (password == "")     //password should not be empty
                {
                    $(".passwordErr").text("please enter password");
                    return false;
                }
                if (password.length < 6)    //password minimum length is 6 characters
                {
                    $(".passwordErr").text("minimum 6 characters");
                    return false;
                }
                if (password.length > 16)   //password maximum length is 16 characters
                {
                    $(".passwordErr").text("maximum 16 characters");
                    return false;
                }
                else {
                    $(".passwordErr").text("");
                }
            });
            $(".launchPath").keyup(function () {
                var locpath = $(".launchPath").val();    //gets the value of class locpath
                if (locpath == "")     //locpath should not be empty
                {
                    $(".launchPathErr").text("please enter launch path");
                    return false;
                }
                else {
                    $(".launchPathErr").text("");
                }
            });
            $(".validate").click(function () {
                var count = 0;
                var ftppath = $(".ftppath").val();
                var username = $(".username").val();
                var password = $(".password").val();
                var name = $(".name").val();
                var launchPath = $(".launchPath").val();
                var ftpstartstring = ftppath.substring(0, 6);
                if (ftpstartstring != "ftp://") {
                    $(".ftppathErr").text("please correct the path. 'eg: ftp://'");
                    $(".ftppath").val("ftp://");
                    count = count + 1;
                }
                if (ftppath == "" || ftppath == "ftp://")     //ftppath should not be empty
                {
                    $(".ftppathErr").text("please enter ftp path");
                    $(".ftppath").val("ftp://");
                    count = count + 1;
                }
                if (name == "")
                {
                    $(".nameErr").text("please enter pre-requisite name");
                    count = count + 1;
                }
                if (username.length > 40)   //username maximum length is 40 characters
                {
                    $(".usernameErr").text("maximum 40 characters");
                    count = count + 1;
                }
                if (username == "")     //username should not be empty
                {
                    $(".usernameErr").text("please enter username");
                    count = count + 1;
                }
                if (password.length < 6)    //password minimum length is 6 characters
                {
                    $(".passwordErr").text("minimum 6 characters");
                    count = count + 1;
                }
                if (password.length > 16)   //password maximum length is 16 characters
                {
                    $(".passwordErr").text("maximum 16 characters");
                    count = count + 1;
                }
                if (password == "")     //password should not be empty
                {
                    $(".passwordErr").text("please enter password");
                    count = count + 1;
                }
                if (launchPath == "") {
                    $(".launchPathErr").text("please enter Launch path");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>
    <div class="col-md-6 col-sm-6 col-centered" style="margin-top: 4%;">
        <div class="panel panel-info">
            <div class="panel-heading">New Pre-Requisite</div>
            <div class="panel-body">
                <div class="row form-group" id="requisiteName">
                    <div class="col-md-3 col-sm-3">
                        Name
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtprerequisitename" runat="server" class="form-control name"></asp:TextBox>
                        <span class="nameErr error-code"></span>
                    </div>
                </div>
                 <div class="row form-group" id="desc">
                    <div class="col-md-3 col-sm-3">
                        Description
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtDesc" runat="server" class="form-control desc"></asp:TextBox>
                        <span class="descErr error-code"></span>
                    </div>
                </div>
                <div class="row form-group" id="ftpPath">
                    <div class="col-md-3 col-sm-3">
                        FTP Path
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtFtpPath" runat="server" Text="ftp://" class="form-control ftppath" />
                        <span class="ftppathErr error-code"></span>
                    </div>
                </div>

                <div class="row form-group" id="ftpusername">
                    <div class="col-md-3 col-sm-3">
                        FTP UserName
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtFtpusername" runat="server" class="form-control username" />
                        <span class="usernameErr error-code"></span>
                    </div>
                </div>
                <div class="row form-group" id="ftppassword">
                    <div class="col-md-3 col-sm-3">
                        FTP Password
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtftppwd" runat="server" TextMode="Password" class="form-control password"/>
                        <span class="passwordErr error-code"></span>
                    </div>
                </div>
                <div class="row form-group" id="launchpath">
                    <div class="col-md-3 col-sm-3">
                        Launch Path
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtLaunchPath" runat="server" class="form-control launchPath"/>
                        <span class="launchPathErr error-code"></span>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 col-sm-3">
                        <asp:Label ID="Label1" runat="server" Text="Installer Mode"></asp:Label>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:RadioButtonList ID="radInstallerMode" runat="server" Width="300px" RepeatColumns="2" RepeatDirection="Horizontal" Height="20px">
                            <asp:ListItem>Automatic</asp:ListItem>
                            <asp:ListItem>Manual</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 col-sm-3">
                        <asp:Label ID="Label2" runat="server" Text="Extraction Mode" ></asp:Label>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:RadioButtonList ID="radExtraction" runat="server" Width="300px" RepeatColumns="2" RepeatDirection="Horizontal" Height="20px">
                            <asp:ListItem>Enable</asp:ListItem>
                            <asp:ListItem>Disable</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 col-sm-3">
                        <asp:Label ID="lblenabled" runat="server" Text="IsActive" ></asp:Label>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:RadioButtonList ID="radList" runat="server" Width="300px"  RepeatColumns="2" RepeatDirection="Horizontal" Height="20px">
                            <asp:ListItem>Enable</asp:ListItem>
                            <asp:ListItem>Disable</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-3 col-sm-3">
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-primary validate" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnReset" runat="server" Text="Reset" class="btn btn-default"
                            OnClientClick="javascript:return Reset()"
                            OnClick="btnReset_Click" />
                        <asp:Button ID="btnBack" runat="server" Text="Back" class="btn btn-default" 
                            OnClientClick="javascript:return Reset()"
                            OnClick="btnBack_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
