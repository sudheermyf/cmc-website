﻿using TouchonCloudSecurityDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TocCMC
{
    public partial class RegisterApp : System.Web.UI.Page
    {
        object lockTarget = new object();
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {

                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lock (lockTarget)
            {
                AppDB.Logger.Info("Initiating Post Application: " + txtAppName.Text.Trim() + " Started.");
                if (!string.IsNullOrEmpty(txtAppName.Text) && !string.IsNullOrEmpty(txtversion.Text))
                {
                    if (!string.IsNullOrEmpty(txtAppName.Text))
                    {
                        if (!string.IsNullOrEmpty(txtversion.Text))
                        {
                            if (!AppDB.securityDB.RegApps.ToList().Exists(c => c.AppName.Equals(txtAppName.Text.Trim(), StringComparison.OrdinalIgnoreCase)))
                            {
                                AppDB.Logger.Info("Application Doesn't exist. Saving Started.");
                                try
                                {
                                    lock (lockTarget)
                                    {
                                        RegisteredApplications regApp = new RegisteredApplications();
                                        regApp.AppName = txtAppName.Text.Trim();
                                        regApp.Version = txtversion.Text.Trim('.');
                                        regApp.IsUpdateAvailable = true;
                                        regApp.IsActive = true;
                                        regApp.UpdatedTime = DateTime.Now;
                                        AppDB.securityDB.RegApps.Add(regApp);
                                        AppDB.securityDB.SaveChanges();
                                        ClientScript.RegisterStartupScript(GetType(), "notifier1", "successAlert('Application created successfully');", true);
                                        clearAll();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    AppDB.Logger.ErrorException(ex.Message, ex);
                                    ClientScript.RegisterStartupScript(GetType(), "notifier2", "errorAlert('" + ex.Message + "');", true);
                                }
                            }
                            else
                            {
                                AppDB.Logger.Info("Application Already Exists.");
                                ClientScript.RegisterStartupScript(GetType(), "notifier3", "errorAlert('Application already exists');", true);
                            }
                        }
                        else
                        {
                            AppDB.Logger.Info("Version should not be empty.");
                            ClientScript.RegisterStartupScript(GetType(), "notifier4", "warningAlert('Version should not be empty');", true);
                        }
                    }
                    else
                    {
                        AppDB.Logger.Info("Application Name should not be empty.");
                        ClientScript.RegisterStartupScript(GetType(), "notifier5", "warningAlert('Application Name should not be empty');", true);
                    }
                }
                else
                {
                    AppDB.Logger.Info("Required Fields should Not Be Empty.");
                    ClientScript.RegisterStartupScript(GetType(), "notifier6", "errorAlert('Required fields should not be empty');", true);
                }
            }
        }

        private void clearAll()
        {
            txtAppName.Text = "";
            txtversion.Text = "";
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            clearAll();
            ClientScript.RegisterStartupScript(GetType(), "notifier7", "messageAlert('Data has been reset');", true);
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewApps.aspx");
        }
    }
}