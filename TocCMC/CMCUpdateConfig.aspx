﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="CMCUpdateConfig.aspx.cs" Inherits="TocCMC.CMCUpdateConfig" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <%--     <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
   --%>
    <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script type="text/javascript">
        function Reset() {
            $(".configname").val("");
            $(".version").val("");
            $(".ftppath").val("ftp://");
            $(".username").val("");
            $(".password").val("");
            return true;
        }
        $(document).ready(function () {
            $(".configname").keyup(function () {
                var configname = $(".configname").val();    //gets the value of class configname
                var appexpression = /^[a-zA-Z ]+$/;
                if (configname == "")     //configname should not be empty
                {
                    $(".confignameErr").text("please enter configuration name");
                    return false;
                }
                if (!appexpression.test(configname)) {
                    $(".confignameErr").text("special characters, numbers not allowed");
                    return false;
                }
                if (configname.length < 3)    //configname minimum length is 3 characters
                {
                    $(".confignameErr").text("minimum 3 characters");
                    return false;
                }
                if (configname.length > 50)   //configname maximum length is 50 characters
                {
                    $(".confignameErr").text("maximum 50 characters");
                    return false;
                }
                else {
                    $(".confignameErr").text("");
                }
            });
            $(".version").keyup(function () {
                var version = $(".version").val();    //gets the value of class version
                var expression = /^[0-9]+$/;
                if (version == "")     //version should not be empty
                {
                    $(".versionErr").text("please enter version");
                    return false;
                }
                if (!expression.test(version)) {
                    $(".versionErr").text("only numberic values");
                    return false;
                }
                if (version.length < 1)    //version minimum length is 1 characters
                {
                    $(".versionErr").text("minimum 1 characters");
                    return false;
                }
                if (version.length > 3)   //version maximum length is 8 characters
                {
                    $(".versionErr").text("maximum 3 characters");
                    return false;
                }
                else {
                    $(".versionErr").text("");
                }
            });
            $(".ftppath").keyup(function () {
                var ftppath = $(".ftppath").val();    //gets the value of class ftppath            
                var ftpstartstring = ftppath.substring(0,6);
                if (ftppath == "" || ftppath == "ftp://")     //ftppath should not be empty
                {
                    $(".ftppathErr").text("please enter ftppath");
                    $(".ftppath").val("ftp://");
                    return false;
                }
                if (ftpstartstring != "ftp://")
                {
                    $(".ftppathErr").text("please correct the path. 'eg: ftp://'");
                    $(".ftppath").val("ftp://");
                    return false;
                }
                else {
                    $(".ftppathErr").text("");
                }
            });
            $(".username").keyup(function () {
                var username = $(".username").val();    //gets the value of class username
                if (username == "")     //username should not be empty
                {
                    $(".usernameErr").text("please enter username name");
                    return false;
                }
                if (username.length > 40)   //username maximum length is 40 characters
                {
                    $(".usernameErr").text("maximum 40 characters");
                    return false;
                }
                else {
                    $(".usernameErr").text("");
                }
            });
            $(".password").keyup(function () {
                var password = $(".password").val();    //gets the value of class password                
                if (password == "")     //password should not be empty
                {
                    $(".passwordErr").text("please enter password");
                    return false;
                }
                if (password.length < 6)    //password minimum length is 6 characters
                {
                    $(".passwordErr").text("minimum 6 characters");
                    return false;
                }
                if (password.length > 16)   //password maximum length is 16 characters
                {
                    $(".passwordErr").text("maximum 16 characters");
                    return false;
                }
                else {
                    $(".passwordErr").text("");
                }
            });
            $(".validate").click(function () {
                var count = 0;
                var configname = $(".configname").val();
                var version = $(".version").val();
                var ftppath = $(".ftppath").val();
                var username = $(".username").val();
                var password = $(".password").val();
                
                var appexpression = /^[a-zA-Z ]+$/;
                if (!appexpression.test(configname)) {
                    $(".confignameErr").text("special characters, numbers not allowed");
                    count = count + 1;
                }
                if (configname.length < 3)    //configname minimum length is 3 characters
                {
                    $(".confignameErr").text("minimum 3 characters");
                    count = count + 1;
                }
                if (configname.length > 50)   //configname maximum length is 50 characters
                {
                    $(".confignameErr").text("maximum 50 characters");
                    count = count + 1;
                }
                if (configname == "")     //configname should not be empty
                {
                    $(".confignameErr").text("please enter configuration name");
                    count = count + 1;
                }
                if (password.length < 6)    //password minimum length is 6 characters
                {
                    $(".passwordErr").text("minimum 6 characters");
                    count = count + 1;
                }
                if (password.length > 16)   //password maximum length is 16 characters
                {
                    $(".passwordErr").text("maximum 16 characters");
                    count = count + 1;
                }
                if (password == "")     //password should not be empty
                {
                    $(".passwordErr").text("please enter password");
                    count = count + 1;
                }
                if (username.length > 40)   //username maximum length is 40 characters
                {
                    $(".usernameErr").text("maximum 40 characters");
                    count = count + 1;
                }
                if (username == "")     //username should not be empty
                {
                    $(".usernameErr").text("please enter username name");
                    count = count + 1;
                }
                var ftpstartstring = ftppath.substring(0, 6);
                if (ftpstartstring != "ftp://") {
                    $(".ftppathErr").text("please correct the path. 'eg: ftp://'");
                    $(".ftppath").val("ftp://");
                    count = count + 1;
                }
                if (ftppath == "" || ftppath == "ftp://")     //ftppath should not be empty
                {
                    $(".ftppathErr").text("please enter ftppath");
                    $(".ftppath").val("ftp://");
                    count = count + 1;
                }
                var expression = /^[0-9]+$/;
                if (!expression.test(version)) {
                    $(".versionErr").text("only numberic values");
                    count = count + 1;
                }
                if (version.length < 1)    //version minimum length is 1 characters
                {
                    $(".versionErr").text("minimum 1 characters");
                    count = count + 1;
                }
                if (version.length > 3)   //version maximum length is 8 characters
                {
                    $(".versionErr").text("maximum 3 characters");
                    count = count + 1;
                }
                if (version == "")     //version should not be empty
                {
                    $(".versionErr").text("please enter version");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>

    <div class="col-md-6 col-sm-6 col-centered" style="margin-top: 4%;">
        <div class="panel panel-info">
            <div class="panel-heading">CMC Update Configuration</div>
            <div class="panel-body">
                <div class="row form-group" id="configName">
                    <div class="col-md-3 col-sm-3">
                        Configuration Name
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtConfigName" runat="server" class="form-control configname"></asp:TextBox>
                        <span class="confignameErr error-code"></span>
                    </div>
                </div>
                    <div class="row form-group">
                    <div class="col-md-3 col-sm-3">
                        CMC Version
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtCMCVersion" runat="server"  class="form-control version"/>
                        <span class="versionErr error-code"></span>
                    </div>
                </div>
                <div class="row form-group" id="ftpPath">
                    <div class="col-md-3 col-sm-3">
                        FTP Path
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtFtpPath" runat="server" Text="ftp://" class="form-control ftppath" />
                        <span class="ftppathErr error-code"></span>
                    </div>
                </div>

                <div class="row form-group" id="ftpusername">
                    <div class="col-md-3 col-sm-3">
                        FTP UserName
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtFtpusername" runat="server" class="form-control username" />
                        <span class="usernameErr error-code"></span>
                    </div>
                </div>
                <div class="row form-group" id="ftppassword">
                    <div class="col-md-3 col-sm-3">
                        FTP Password
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtftppwd" runat="server" TextMode="Password" class="form-control password"/>
                        <span class="passwordErr error-code"></span>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 col-sm-3">
                        <asp:Label ID="lblenabled" runat="server" Text="CMC Status" Visible="false"></asp:Label>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:RadioButtonList ID="radList" runat="server" Width="300px" Visible="false" RepeatColumns="2" RepeatDirection="Horizontal" Height="20px">
                            <asp:ListItem>Enable</asp:ListItem>
                            <asp:ListItem>Disable</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-3 col-sm-3">
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-primary validate" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnReset" runat="server" Text="Reset" class="btn btn-default"
                            OnClientClick="javascript:return Reset()"
                            OnClick="btnReset_Click" />
                        <asp:Button ID="btnBack" runat="server" Text="Back" class="btn btn-default" 
                            OnClientClick="javascript:return Reset()"
                            OnClick="btnBack_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
