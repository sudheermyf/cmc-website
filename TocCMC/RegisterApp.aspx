﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="RegisterApp.aspx.cs" Inherits="TocCMC.RegisterApp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script>
        $(document).ready(function () {
            $(".appname").keyup(function () {
                var appname = $(".appname").val();    //gets the value of class appname
                if (appname == "")     //appname should not be empty
                {
                    $(".appnameErr").text("please enter app name");
                    return false;
                }
                if (appname.length < 3)    //appname minimum length is 3 characters
                {
                    $(".appnameErr").text("minimum 3 characters");
                    return false;
                }
                if (appname.length > 50)   //appname maximum length is 50 characters
                {
                    $(".appnameErr").text("maximum 50 characters");
                    return false;
                }
                else {
                    $(".appnameErr").text("");
                }
            });
            $(".version").keyup(function () {
                var version = $(".version").val();    //gets the value of class version
                var expression = /^[0-9a-zA-Z.]+$/;
                if (version == "")     //version should not be empty
                {
                    $(".versionErr").text("please enter version");
                    return false;
                }
                if (!expression.test(version)) {
                    $(".versionErr").text("invalid expression");
                    return false;
                }
                if (version.length < 1)    //version minimum length is 1 characters
                {
                    $(".versionErr").text("minimum 1 characters");
                    return false;
                }
                if (version.length > 8)   //version maximum length is 8 characters
                {
                    $(".versionErr").text("maximum 8 characters");
                    return false;
                }
                else {
                    $(".versionErr").text("");
                }
            });
            $(".Validate").click(function () {
                var count = 0;
                var appname = $(".appname").val();    //gets the value of class appname
                
                if (appname.length < 3)    //appname minimum length is 3 characters
                {
                    $(".appnameErr").text("minimum 3 characters");
                    count = count + 1;
                }
                if (appname.length > 50)   //appname maximum length is 50 characters
                {
                    $(".appnameErr").text("maximum 50 characters");
                    count = count + 1;
                }
                if (appname == "")     //appname should not be empty
                {
                    $(".appnameErr").text("please enter app name");
                    count = count + 1;
                }
                var version = $(".version").val();    //gets the value of class version
                var expression = /^[0-9a-zA-Z.]+$/;
                if (!expression.test(version)) {
                    $(".versionErr").text("invalid expression");
                    count = count + 1;
                }
                if (version.length < 1)    //version minimum length is 1 characters
                {
                    $(".versionErr").text("minimum 1 characters");
                    count = count + 1;
                }
                if (version.length > 8)   //version maximum length is 8 characters
                {
                    $(".versionErr").text("maximum 8 characters");
                    count = count + 1;
                }
                if (version == "")     //version should not be empty
                {
                    $(".versionErr").text("please enter version");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>
    <div class="col-md-6 col-sm-5 col-centered" style="margin-top: 9%;">
        <div class="panel panel-info">
            <div class="panel-heading">Register App</div>
            <div class="panel-body">
                <div class="row form-group" id="appName">
                    <div class="col-md-3 col-sm-3">
                        Application Name
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtAppName" runat="server" class="form-control appname" />
                        <span class="appnameErr error-code"></span>
                    </div>
                </div>
                <div class="row form-group" id="appVersion">
                    <div class="col-md-3 col-sm-3">
                        Version Number
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:TextBox ID="txtversion" runat="server" class="form-control version" />
                        <span class="versionErr error-code"></span>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 col-sm-3">
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-primary Validate" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnReset" runat="server" Text="Reset" class="btn btn-default" CausesValidation="false" OnClick="btnReset_Click" />
                        <asp:Button ID="btnBack" runat="server" Text="Back" class="btn btn-default" CausesValidation="false" OnClick="btnBack_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
