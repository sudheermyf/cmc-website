﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TocCMC
{
    public partial class LinkAppWithPrerequisite : System.Web.UI.Page
    {
        object lockTarget = new object();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    lock (lockTarget)
                    {
                        AppDB.Logger.Info("Initiating App Update.");
                        //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                        AppDB.Logger.Info("Checking for Atleast One Application Name exists in DB..");
                        if (AppDB.securityDB.RegApps.ToList().Count > 0)
                        {
                            AppDB.Logger.Info("Fetching all the applications and bind to the dropdownlist");
                            ddlappname.DataSource = AppDB.securityDB.RegApps.ToList();
                            ddlappname.DataBind();
                        }
                        if(AppDB.securityDB.RegisterPrerequisites.ToList().Count>0)
                        {
                            chkPrerequisite.DataSource = AppDB.securityDB.RegisterPrerequisites.ToList();
                            chkPrerequisite.DataBind();
                        }
                    }
                }
            }
        }
        protected void ddlappname_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlappname.SelectedItem.Text != "Select")
            {
                string strName = ddlappname.SelectedItem.Text;
                TouchonCloudSecurityDB.Models.RegisteredApplications regApp = AppDB.securityDB.RegApps.FirstOrDefault(c => c.AppName.Equals(strName));
                if (regApp != null && regApp.LstStoreAdmins != null && regApp.LstStoreAdmins.Count > 0)
                {
                    ddlStores.DataSource = regApp.LstStoreAdmins;
                    ddlStores.DataBind();
                }
            }
            else
            {
                ddlStores.DataSource = null;
                ddlStores.DataBind();
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            clearAll();
        }

        private void clearAll()
        {
            ddlKiosk.Items.Clear();
            ddlKiosk.Items.Add("Select");
            ddlStores.Items.Clear();
            ddlStores.Items.Add("Select");
            ddlappname.ClearSelection();
            foreach (ListItem item in chkPrerequisite.Items)
            {
                item.Selected = false;
            }

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            TouchonCloudSecurityDB.Models.SecurityKey secKey = AppDB.securityDB.SecurityKey.Where(c => c.SysName.Equals(ddlKiosk.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            foreach (ListItem item in chkPrerequisite.Items)
            {
                TouchonCloudSecurityDB.Models.RegisterPrerequisite prerequisite = AppDB.securityDB.RegisterPrerequisites.FirstOrDefault(c => c.Name.Equals(item.Text));
                if(secKey.LSTPreRequestisites.ToList().Contains(prerequisite))
                {
                    if(item.Selected)
                    {

                    }
                    else
                    {
                        secKey.LSTPreRequestisites.Remove(prerequisite);
                    }
                }
                else
                {
                    secKey.LSTPreRequestisites.Add(prerequisite);
                }
            }
            AppDB.securityDB.SaveChanges();
            ClientScript.RegisterStartupScript(GetType(), "notifier6", "successAlert('Pre-Requisite Updated Successfully');", true);
        }

        protected void ddlStores_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStores.SelectedItem.Text != "Select")
            {
                lock (lockTarget)
                {
                    AppDB.Logger.Info("Fetching the StoreAdmin according to the selected..ddlstores " + ddlStores.SelectedItem.Text + "");
                    //TouchonCloudSecurityDB.Models.SecurityKey secKey = AppDB.securityDB.SecurityKey.Where(c => c.StoreAdmin.UserName.Equals(ddlstores.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    TouchonCloudSecurityDB.Models.StoreAdmin StoreAdmin = AppDB.securityDB.StoreAdmin.Where(c => c.UserName.Equals(ddlStores.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (StoreAdmin != null)
                    {

                        AppDB.Logger.Info("Fetching all the SysName's according to the selected store..ddlstores" + ddlStores.SelectedItem.Text + "");
                        ddlKiosk.DataSource = StoreAdmin.LstRegKeys.ToList();
                        ddlKiosk.DataBind();
                    }
                }
            }
            else
            {
                ddlKiosk.Items.Clear();
                ddlKiosk.Items.Add("Select");
            }
        }

        protected void ddlKiosk_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlKiosk.SelectedItem.Text != "Select")
            {
                TouchonCloudSecurityDB.Models.SecurityKey secKey = AppDB.securityDB.SecurityKey.Where(c => c.SysName.Equals(ddlKiosk.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                secKey.LSTPreRequestisites.ToList().ForEach(c =>
                {
                    foreach (ListItem item in chkPrerequisite.Items)
                    {
                        if(item.Text.Equals(c.Name))
                        {
                            item.Selected = true;
                        }
                    }                   
                });
            }
        }
    }
}