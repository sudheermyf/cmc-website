﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="PrerequisiteView.aspx.cs" Inherits="TocCMC.PrerequisiteView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <script type="text/javascript">
         $(document).ready(function () {
             $(".ftppath").keyup(function () {
                 var ftppath = $(".ftppath").val();    //gets the value of class ftppath            
                 var ftpstartstring = ftppath.substring(0, 6);
                 if (ftppath == "" || ftppath == "ftp://")     //ftppath should not be empty
                 {
                     $(".ftppathErr").text("please enter ftppath");
                     $(".ftppath").val("ftp://");
                     return false;
                 }
                 if (ftpstartstring != "ftp://") {
                     $(".ftppathErr").text("please correct the path. 'eg: ftp://'");
                     $(".ftppath").val("ftp://");
                     return false;
                 }
                 else {
                     $(".ftppathErr").text("");
                 }
             });
             $(".username").keyup(function () {
                 var username = $(".username").val();    //gets the value of class username
                 if (username == "")     //username should not be empty
                 {
                     $(".usernameErr").text("please enter username name");
                     return false;
                 }
                 if (username.length > 40)   //username maximum length is 40 characters
                 {
                     $(".usernameErr").text("maximum 40 characters");
                     return false;
                 }
                 else {
                     $(".usernameErr").text("");
                 }
             });
             $(".password").keyup(function () {
                 var password = $(".password").val();    //gets the value of class password                
                 if (password == "")     //password should not be empty
                 {
                     $(".passwordErr").text("please enter password");
                     return false;
                 }
                 if (password.length < 6)    //password minimum length is 6 characters
                 {
                     $(".passwordErr").text("minimum 6 characters");
                     return false;
                 }
                 if (password.length > 16)   //password maximum length is 16 characters
                 {
                     $(".passwordErr").text("maximum 16 characters");
                     return false;
                 }
                 else {
                     $(".passwordErr").text("");
                 }
             });
             $(".updatevalidate").click(function () {
                 var count = 0;
                 var ftppath = $(".ftppath").val();
                 var username = $(".username").val();
                 var password = $(".password").val();

                 if (password.length < 6)    //password minimum length is 6 characters
                 {
                     $(".passwordErr").text("minimum 6 characters");
                     count = count + 1;
                 }
                 if (password.length > 16)   //password maximum length is 16 characters
                 {
                     $(".passwordErr").text("maximum 16 characters");
                     count = count + 1;
                 }
                 if (password == "")     //password should not be empty
                 {
                     $(".passwordErr").text("please enter password");
                     count = count + 1;
                 }
                 if (username.length > 40)   //username maximum length is 40 characters
                 {
                     $(".usernameErr").text("maximum 40 characters");
                     count = count + 1;
                 }
                 if (username == "")     //username should not be empty
                 {
                     $(".usernameErr").text("please enter username");
                     count = count + 1;
                 }
                 var ftpstartstring = ftppath.substring(0, 6);                 
                 if (ftpstartstring != "ftp://") {
                     $(".ftppathErr").text("please correct the path. 'eg: ftp://'");
                     $(".ftppath").val("ftp://");
                     count = count + 1;
                 }
                 if (ftppath == "" || ftppath == "ftp://")     //ftppath should not be empty
                 {
                     $(".ftppathErr").text("please enter ftppath");
                     $(".ftppath").val("ftp://");
                     count = count + 1;
                 }
                 if (count > 0) {
                     return false;
                 }
             });
         });
         function Validate() {
             var gv = document.getElementById("<%=gdviewrequisite.ClientID%>");
            var rbs = gv.getElementsByTagName("input");
            var flag = 0;
            for (var i = 0; i < rbs.length; i++) {

                if (rbs[i].type == "radio") {
                    if (rbs[i].checked) {
                        flag = 1;
                        break;
                    }
                }
            }
            if (flag == 0) {
                warningAlert('Please Select Anyone');
                return false;
            }
            else {
                var x = confirm("Are you sure you want to delete?");
                if (x == true)
                    return true;
            }
        }
    </script>
    <script type="text/javascript">
        function SelectSingleRadiobutton(rdbtnid) {
            var rdBtn = document.getElementById(rdbtnid);
            var rdBtnList = document.getElementsByTagName("input");
            for (i = 0; i < rdBtnList.length; i++) {
                if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                    rdBtnList[i].checked = false;
                }
            }
        }
    </script>
    <div class="container col-xs-12">
        <div id="mainContainer">
            <div id="tblPanel">
                <div class="col-md-2 col-sm-6">
                    <div class="list-group">
                        <div class="list-group-item">
                             <asp:Button ID="btnPreRequisite" runat="server" OnClick="btnPreRequisite_Click" class="btn btn-primary" Style="width:100%;font-size:10pt;" Text="New PreRequisite"/>                          
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-sm-6">
                    <div class="table-responsive">
                        <asp:GridView ID="gdviewrequisite" runat="server" AutoGenerateColumns="False"
                            AllowSorting="true" OnSorting="gdviewrequisite_Sorting"
                            class="table table-hover table-bordered table-striped">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:RadioButton ID="RadioButton1" runat="server" OnCheckedChanged="RadioButton1_CheckedChanged"
                                            OnClick="javascript:SelectSingleRadiobutton(this.id)" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="strSno" HeaderText="SNo" Visible="false" />
                                <asp:BoundField DataField="strName" HeaderText="Name" SortExpression="strName"/>
                                <asp:BoundField DataField="strFtpUserName" HeaderText="FTP User Name" SortExpression="strFtpUserName" />
                                <asp:BoundField DataField="strUpdatedTime" HeaderText="Updated Time" SortExpression="strUpdatedTime" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <asp:Button ID="btnEditPrerequisite" runat="server" Text="Edit" class="btn btn-primary" OnClick="btnEditPrerequisite_Click" 
                        data-toggle="modal" data-target="#myModalViewCMC"/>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CausesValidation="false"
                        OnClientClick="return Validate();" OnClick="btnDelete_Click" class="btn btn-danger" />
                </div>
            </div>

            <div class="modal fade" id="myModalViewPrerequisite" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Edit Pre-Requisite</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    SNo
                                </div>
                                <div class="col-md-6 col-sm-6">
                                     <asp:Label ID="lblid" runat="server" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                   Name
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtName" runat="server" class="form-control"/>
                                </div>
                            </div>
                          <div class="row form-group">
                                <div class="col-md-4 col-sm-6">Description                                 
                                </div>
                                <div class="col-md-6 col-sm-6"><asp:TextBox ID="txtDesc" runat="server" class="form-control" ReadOnly="true"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">FTP Path
                                </div>
                                <div class="col-md-6 col-sm-6"><asp:TextBox ID="txtftppath" class="form-control ftppath" runat="server" />
                                    <span class="ftppathErr error-code"></span>
                                </div>
                            </div>
                             <div class="row form-group">
                                <div class="col-md-4 col-sm-6">FTP UserName
                                </div>
                                <div class="col-md-6 col-sm-6"><asp:TextBox ID="txtftpusername" class="form-control username"  runat="server" />
                                    <span class="usernameErr error-code"></span>
                                </div>
                            </div>

                             <div class="row form-group">
                                <div class="col-md-4 col-sm-6">FTP Password
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtftppwd" class="form-control password" runat="server" />
                                    <span class="passwordErr error-code"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">Launch Path
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtLaunchPath" class="form-control password" runat="server" />
                                </div>
                            </div>
                           
                             <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                </div>
                                <div class="col-md-6 col-sm-6">

                                    <asp:RadioButtonList ID="radInstallerMode" runat="server" Width="300px" RepeatColumns="2" RepeatDirection="Horizontal" Height="20px">
                                        <asp:ListItem>Automatic</asp:ListItem>
                                        <asp:ListItem>Manual</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>  
                             <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                </div>
                                 <div class="col-md-6 col-sm-6">
                                     <asp:RadioButtonList ID="radExtraction" runat="server" Width="300px" RepeatColumns="2" RepeatDirection="Horizontal" Height="20px">
                                         <asp:ListItem>Enable</asp:ListItem>
                                         <asp:ListItem>Disable</asp:ListItem>
                                     </asp:RadioButtonList>
                                 </div>
                            </div>  
                             <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                </div>
                                 <div class="col-md-6 col-sm-6">
                                     <asp:RadioButtonList ID="radList" runat="server" Width="300px" RepeatColumns="2" RepeatDirection="Horizontal" Height="20px">
                                         <asp:ListItem>Enable</asp:ListItem>
                                         <asp:ListItem>Disable</asp:ListItem>
                                     </asp:RadioButtonList>
                                 </div>
                            </div>                                             
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update" class="btn btn-primary updatevalidate" OnClick="btnUpdate_Click"/>
                            <asp:Button ID="btnCancel" runat="server" Text="Reset" CausesValidation="false" class="btn btn-default"
                                 OnClick="btnCancel_Click" />
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

        </div>
    </div>
</asp:Content>
