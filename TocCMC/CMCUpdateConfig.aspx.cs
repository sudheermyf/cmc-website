﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TocCMC
{
    public partial class CMCUpdateConfig : System.Web.UI.Page
    {
        object lockTarget = new object();
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {

                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AppDB.Logger.Info("Initiating Post Application Configuration: " + txtConfigName.Text + " Started.");
            if (!string.IsNullOrEmpty(txtConfigName.Text)  && !string.IsNullOrEmpty(txtftppwd.Text) && !string.IsNullOrEmpty(txtFtpPath.Text) && !string.IsNullOrEmpty(txtFtpusername.Text))
            {
                AppDB.Logger.Info("Validating Application configuration exists in DB or not.");
                if (!AppDB.securityDB.CMCUpdates.ToList().Exists(c => c.ConfigurationName.Equals(txtConfigName.Text, StringComparison.OrdinalIgnoreCase)))
                {
                    AppDB.Logger.Info("Validating Application Configuration exists");

                   
                        try
                        {
                            lock (lockTarget)
                            {
                                TouchonCloudSecurityDB.Models.CMCUpdateConfiguration cmcconfig = new TouchonCloudSecurityDB.Models.CMCUpdateConfiguration();
                                cmcconfig.ConfigurationName = txtConfigName.Text.Trim();
                                cmcconfig.FtpPath = txtFtpPath.Text.Trim();
                                cmcconfig.CMCVersion = Convert.ToInt32(txtCMCVersion.Text);
                                cmcconfig.FtpUserName = txtFtpusername.Text.Trim();
                                cmcconfig.FtpPassword = txtftppwd.Text.Trim();
                                cmcconfig.IsActive = true;
                                cmcconfig.ServerDownloadPath = cmcconfig.FtpPath;
                                cmcconfig.UpdatedTime = DateTime.Now;
                                AppDB.securityDB.CMCUpdates.Add(cmcconfig);
                                AppDB.securityDB.SaveChanges();
                                clearAll();
                                ClientScript.RegisterStartupScript(GetType(), "notifier", "successAlert('Configuration Successfully Saved');", true);
                            
                            }
                            LogHelper.Logger.Info("Ended Submit Button in CMC Configuration");
                        }
                        catch (Exception ex)
                        {
                            AppDB.Logger.ErrorException(ex.Message, ex);
                            ClientScript.RegisterStartupScript(GetType(), "notifier2", "errorAlert('" + ex.Message + "');", true);
                        }
                }
                else
                {
                    AppDB.Logger.Info("Application Configuration already registered.");
                    ClientScript.RegisterStartupScript(GetType(), "notifier4", "errorAlert('CMC Name already existed');", true);
                }
            }
            else
                ClientScript.RegisterStartupScript(GetType(), "notifier5", "warningAlert('Required fields should not be empty');", true);

        }
        private void clearAll()
        {
            txtConfigName.Text = "";
            txtFtpPath.Text = "ftp://";
            txtftppwd.Text = "";
            txtFtpusername.Text = "";
            txtCMCVersion.Text = "";
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            clearAll();
            ClientScript.RegisterStartupScript(GetType(), "notifier6", "messageAlert('Data has been reset');", true);
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            clearAll();
            Response.Redirect("CMCUpdateView.aspx");
        }
    }
}