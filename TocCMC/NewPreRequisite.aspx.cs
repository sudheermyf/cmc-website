﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TocCMC
{
    public partial class NewPreRequisite : System.Web.UI.Page
    {
        object lockTarget = new object();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {

                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AppDB.Logger.Info("Initiating Post Application Pre-requisite: " + txtprerequisitename.Text + " Started.");
            if (!string.IsNullOrEmpty(txtprerequisitename.Text) && !string.IsNullOrEmpty(txtftppwd.Text) && !string.IsNullOrEmpty(txtFtpPath.Text) && !string.IsNullOrEmpty(txtFtpusername.Text))
            {
                AppDB.Logger.Info("Validating Application prerequisite exists in DB or not.");
                if (!AppDB.securityDB.RegisterPrerequisites.ToList().Exists(c => c.Name.Equals(txtprerequisitename.Text, StringComparison.OrdinalIgnoreCase)))
                {
                    AppDB.Logger.Info("Validating prerequisite exists");
                    try
                    {
                        lock (lockTarget)
                        {
                            TouchonCloudSecurityDB.Models.RegisterPrerequisite regPrerequisite = new TouchonCloudSecurityDB.Models.RegisterPrerequisite();
                            regPrerequisite.Name = txtprerequisitename.Text.Trim();
                            regPrerequisite.Description = txtDesc.Text;
                            regPrerequisite.FtpUserName = txtFtpusername.Text.Trim();
                            regPrerequisite.FtpPath = txtFtpPath.Text.Trim();
                            regPrerequisite.FtpPassword = txtftppwd.Text.Trim();
                            if (radInstallerMode.SelectedItem.Text.Equals("Automatic"))
                            {
                                regPrerequisite.InstallerMode = TouchonCloudSecurityDB.Enums.InstallerMode.Automatic;
                            }
                            else
                            {
                                regPrerequisite.InstallerMode = TouchonCloudSecurityDB.Enums.InstallerMode.Manual;
                            }
                            regPrerequisite.LaunchPath = txtLaunchPath.Text.Trim();
                            if(radExtraction.SelectedItem.Text.Equals("Enable"))
                            {
                                regPrerequisite.ExtractEnabled="Y";
                            }
                            else
                            {
                                regPrerequisite.ExtractEnabled="N";
                            }
                            if(radList.SelectedItem.Text.Equals("Enable"))
                            {
                                regPrerequisite.IsActive = true;
                            }
                            else
                            {
                                regPrerequisite.IsActive = false;
                            }
                            regPrerequisite.UpdatedTime = DateTime.Now;
                            AppDB.securityDB.RegisterPrerequisites.Add(regPrerequisite);
                            AppDB.securityDB.SaveChanges();
                            clearAll();
                            ClientScript.RegisterStartupScript(GetType(), "notifier", "successAlert('Pre-Requisite Successfully Saved');", true);

                        }
                        LogHelper.Logger.Info("Ended Submit Button in Pre-Requisite");
                    }
                    catch (Exception ex)
                    {
                        AppDB.Logger.ErrorException(ex.Message, ex);
                        ClientScript.RegisterStartupScript(GetType(), "notifier2", "errorAlert('" + ex.Message + "');", true);
                    }
                }
                else
                {
                    AppDB.Logger.Info("Pre-Requisite already registered.");
                    ClientScript.RegisterStartupScript(GetType(), "notifier4", "errorAlert('Name already existed');", true);
                }
            }
            else
                ClientScript.RegisterStartupScript(GetType(), "notifier5", "warningAlert('Required fields should not be empty');", true);
        }

        private void clearAll()
        {
            txtDesc.Text = string.Empty;
            txtFtpPath.Text = "ftp://";
            txtftppwd.Text = string.Empty;
            txtFtpusername.Text = string.Empty;
            txtLaunchPath.Text = string.Empty;
            txtprerequisitename.Text = string.Empty;
            radExtraction.ClearSelection();
            radInstallerMode.ClearSelection();
            radList.ClearSelection();
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            clearAll();
            Response.Redirect("PrerequisiteView.aspx");
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            clearAll();
            ClientScript.RegisterStartupScript(GetType(), "notifier6", "messageAlert('Data has been reset');", true);
        }
    }
}