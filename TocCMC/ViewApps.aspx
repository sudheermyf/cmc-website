﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ViewApps.aspx.cs" Inherits="TocCMC.ViewApps" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">
        function isDelete() {
            return confirm("Do you want to delete this details ?");
        }
        $(document).ready(function () {
            $(".version").keyup(function () {
                var version = $(".version").val();    //gets the value of class version
                var expression = /^[0-9a-zA-Z.]+$/;
                if (version == "")     //version should not be empty
                {
                    $(".versionErr").text("please enter version");
                    return false;
                }
                if (!expression.test(version)) {
                    $(".versionErr").text("invalid expression");
                    return false;
                }
                if (version.length < 1)    //version minimum length is 1 characters
                {
                    $(".versionErr").text("minimum 1 characters");
                    return false;
                }
                if (version.length > 8)   //version maximum length is 8 characters
                {
                    $(".versionErr").text("maximum 8 characters");
                    return false;
                }
                else {
                    $(".versionErr").text("");
                }
            });
            $(".updatevalidate").click(function () {
                var count = 0;               
                var version = $(".version").val();    //gets the value of class version
                var expression = /^[0-9a-zA-Z.]+$/;
                if (!expression.test(version)) {
                    $(".versionErr").text("invalid expression");
                    count = count + 1;
                }
                if (version.length < 1)    //version minimum length is 1 characters
                {
                    $(".versionErr").text("minimum 1 characters");
                    count = count + 1;
                }
                if (version.length > 8)   //version maximum length is 8 characters
                {
                    $(".versionErr").text("maximum 8 characters");
                    count = count + 1;
                }
                if (version == "")     //version should not be empty
                {
                    $(".versionErr").text("please enter version");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>

    <script type="text/javascript">
        function Validate() {
            var gv = document.getElementById("<%=gdviewapp.ClientID%>");
            var rbs = gv.getElementsByTagName("input");
            var flag = 0;
            for (var i = 0; i < rbs.length; i++) {

                if (rbs[i].type == "radio") {
                    if (rbs[i].checked) {
                        flag = 1;
                        break;
                    }
                }
            }
            if (flag == 0) {
                warningAlert('Please Select Anyone');
                return false;
            }
            else {
                var x = confirm("Are you sure you want to delete?");
                if (x == true)
                    return true;
                else {
                    if (document.getElementById("<%=Label1.ClientID%>") != null)
                        document.getElementById("<%=Label1.ClientID%>").innerText = "";
                    return false;
                }
            }
        }
    </script>
    <script type="text/javascript">
        function SelectSingleRadiobutton(rdbtnid) {
            var rdBtn = document.getElementById(rdbtnid);
            var rdBtnList = document.getElementsByTagName("input");
            for (i = 0; i < rdBtnList.length; i++) {
                if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                    rdBtnList[i].checked = false;
                }
            }
        }
    </script>
    <div class="container col-xs-12">
        <div id="mainContainer">
            <div id="tblPanel">
                <div class="col-md-2 col-sm-6">
                    <div class="list-group">
                        <div class="list-group-item">
                            <asp:Button ID="btnNew" runat="server" Text="New Application" class="btn btn-primary"
                                CausesValidation="false" OnClick="btnNew_Click" Style="width: 100%;" />
                            <asp:Label ID="Label1" runat="server"></asp:Label>
                        </div>
                        <div class="list-group-item">
                            <asp:DropDownList ID="ddlappname" AppendDataBoundItems="true" DataTextField="strAppName"
                                DataValueField="strSno" AutoPostBack="true" OnSelectedIndexChanged="ddlappname_SelectedIndexChanged"
                                runat="server" class="form-control">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-sm-6">
                    <div class="table-responsive">
                        <asp:GridView ID="gdviewapp" runat="server" AutoGenerateColumns="False" AllowSorting="true" 
                            OnSorting="gdviewapp_Sorting"
                            OnRowDeleting="gdviewapp_RowDeleting" class="table table-hover table-bordered table-striped">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:RadioButton ID="RadioButton1" runat="server" OnCheckedChanged="RadioButton1_CheckedChanged" 
                                            OnClick="javascript:SelectSingleRadiobutton(this.id)" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="strAppName" HeaderText="Application Name" SortExpression="strAppName" />
                                <asp:BoundField DataField="strVerison" HeaderText="Version" SortExpression="strVerison" />
                                <asp:BoundField DataField="strUpdatedTime" HeaderText="Updated Time" SortExpression="strUpdatedTime" />
                                <asp:BoundField DataField="strUpdateAvail" HeaderText="Update Status" SortExpression="strUpdateAvail" />
                                <asp:BoundField DataField="strIsActive" HeaderText="Service Status" SortExpression="strIsActive" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <asp:Button ID="btnEdit" runat="server" Text="Edit" class="btn btn-primary"
                        data-toggle="modal" data-target="#myModal"
                        OnClick="btnEdit_Click" />
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" class="btn btn-danger"
                        OnClientClick="return Validate();" OnClick="btnDelete_Click" />
                </div>
            </div>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Edit App Details</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    App Name:
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtappname" runat="server" class="form-control" ReadOnly="true"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Last Updated Date:
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtupdatedtime" runat="server" class="form-control" ReadOnly="true"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Version Number:
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtversion" runat="server" class="form-control version" />
                                    <span class="versionErr error-code"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    <asp:Label ID="lblupdateavail" runat="server" Text="Update Status" Visible="false"/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:RadioButtonList ID="radList" runat="server" Width="300px" RepeatColumns="2" Visible="false" AutoPostBack="true" RepeatDirection="Horizontal" Height="20px">
                                        <asp:ListItem>Active</asp:ListItem>
                                        <asp:ListItem>Inactive</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnUpdate" CommandName="Update" class="btn btn-primary updatevalidate" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                            <asp:Button ID="btnReset" runat="server" Text="Reset" class="btn btn-default" OnClick="btnReset_Click" />
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->            
        </div>
    </div>
</asp:Content>
