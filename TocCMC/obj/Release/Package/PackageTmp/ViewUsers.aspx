﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ViewUsers.aspx.cs" Inherits="TocCMC.ViewUsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {
            $(".username").keyup(function () {
                var username = $(".username").val();    //gets the value of class username
                if (username == "")     //username should not be empty
                {
                    $(".usernameErr").text("please enter username name");
                    return false;
                }
                if (username.length > 40)   //username maximum length is 40 characters
                {
                    $(".usernameErr").text("maximum 40 characters");
                    return false;
                }
                else {
                    $(".usernameErr").text("");
                }
            });
            $(".password").keyup(function () {
                var password = $(".password").val();    //gets the value of class password                
                if (password == "")     //password should not be empty
                {
                    $(".passwordErr").text("please enter password");
                    return false;
                }
                if (password.length < 6)    //password minimum length is 6 characters
                {
                    $(".passwordErr").text("minimum 6 characters");
                    return false;
                }
                if (password.length > 16)   //password maximum length is 16 characters
                {
                    $(".passwordErr").text("maximum 16 characters");
                    return false;
                }
                else {
                    $(".passwordErr").text("");
                }
            });
            $(".contactnumber").keyup(function () {
                var contactnumber = $(".contactnumber").val();    //gets the value of class contactnumber  
                var contactexpression = /^[789][0-9]{9}$/;
                ///^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1}){0,1}98(\s){0,1}(\-){0,1}(\s){0,1}[1-9]{1}[0-9]{7}$/;;
                if (!contactexpression.test(contactnumber)) {
                    $(".contactnumberErr").text("enter valid phone number");
                    return false;
                }
                if (contactnumber == "")     //contactnumber should not be empty
                {
                    $(".contactnumberErr").text("please enter contactnumber");
                    return false;
                }
                else {
                    $(".contactnumberErr").text("");
                }
            });
            $(".updatevalidate").click(function () {
                var count = 0;
                var username = $(".username").val();
                var password = $(".password").val();
                var contactnumber = $(".contactnumber").val();

                if (username.length > 40)   //username maximum length is 40 characters
                {
                    $(".usernameErr").text("maximum 40 characters");
                    count = count + 1;
                }
                if (username == "") {
                    $(".usernameErr").text("please enter username");
                    count = count + 1;
                }
                if (password.length < 6)    //password minimum length is 6 characters
                {
                    $(".passwordErr").text("minimum 6 characters");
                    count = count + 1;
                }
                if (password.length > 16)   //password maximum length is 16 characters
                {
                    $(".passwordErr").text("maximum 16 characters");
                    count = count + 1;
                }
                if (password == "") {
                    $(".passwordErr").text("please enter password");
                    count = count + 1;
                }
                var contactexpression = /^[789][0-9]{9}$/;
                if (!contactexpression.test(contactnumber)) {
                    $(".contactnumberErr").text("enter valid phone number");
                    count = count + 1;
                }
                if (contactnumber == "")     //contactnumber should not be empty
                {
                    $(".contactnumberErr").text("please enter contactnumber");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });

		function Validate() {
			var gv = document.getElementById("<%=gdviewusers.ClientID%>");
			var rbs = gv.getElementsByTagName("input");
			var flag = 0;
			for (var i = 0; i < rbs.length; i++) {

				if (rbs[i].type == "radio") {
					if (rbs[i].checked) {
						flag = 1;
						break;
					}
				}
			}
			if (flag == 0) {
				warningAlert('Please Select Anyone');
				return false;
			}
			else {
				var x = confirm("Are you sure you want to delete?");
				if (x == true)
					return true;
				else {
					if (document.getElementById("<%=Label1.ClientID%>") != null)
						 document.getElementById("<%=Label1.ClientID%>").innerText = "";
					 return false;
				 }
			 }
		 }
	</script>
	<script type="text/javascript">
		function SelectSingleRadiobutton(rdbtnid) {
			var rdBtn = document.getElementById(rdbtnid);
			var rdBtnList = document.getElementsByTagName("input");
			for (i = 0; i < rdBtnList.length; i++) {
				if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
					rdBtnList[i].checked = false;
				}
			}
		}
	</script>
	<div class="container col-xs-12">
		<div id="mainContainer">
			<div id="tblPanel">
				<div class="col-md-2 col-sm-6">
					<div class="list-group">
						<div class="list-group-item">
							<asp:Label ID="Label1" runat="server"></asp:Label>
							<asp:Button ID="btnnewuser" runat="server" Text="New User" class="btn btn-primary" Style="width: 100%;" OnClick="btnnewuser_Click" />
						</div>
						<div class="list-group-item">
							<asp:DropDownList ID="ddlappname" AppendDataBoundItems="true" DataTextField="AppName"
								class="form-control" DataValueField="SNo" AutoPostBack="true"
								OnSelectedIndexChanged="ddlappname_SelectedIndexChanged" runat="server">
								<asp:ListItem Value="-1">Select</asp:ListItem>
							</asp:DropDownList>
						</div>
						<div class="list-group-item">
							<asp:DropDownList ID="ddlcmc" AppendDataBoundItems="true" AutoPostBack="true"
								OnSelectedIndexChanged="ddlcmc_SelectedIndexChanged" runat="server" class="form-control">
								<asp:ListItem Value="-1">Select</asp:ListItem>
							</asp:DropDownList>
						</div>
						<div class="list-group-item">
							<asp:DropDownList ID="ddlstorename" AppendDataBoundItems="true" AutoPostBack="true"
								OnSelectedIndexChanged="ddlstorename_SelectedIndexChanged" runat="server" class="form-control">
								<asp:ListItem Value="-1">Select</asp:ListItem>
							</asp:DropDownList>
						</div>
					</div>
				</div>
				<div class="col-md-10 col-sm-6">
					<div class="table-responsive">
						<asp:GridView ID="gdviewusers" runat="server" AutoGenerateColumns="False"
							OnRowDeleting="gdviewusers_RowDeleting" AllowSorting="true"
							OnSorting="gdviewusers_Sorting" class="table table-hover table-bordered table-striped">
							<Columns>
								<asp:TemplateField>
									<ItemTemplate>
										<asp:RadioButton ID="RadioButton1" runat="server" OnCheckedChanged="RadioButton1_CheckedChanged" OnClick="javascript:SelectSingleRadiobutton(this.id)" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField DataField="strSNo" HeaderText="Sno" Visible="false" />
								<asp:BoundField DataField="strAppName" HeaderText="Application Name" SortExpression="strAppName" />
								<asp:BoundField DataField="strConfig" HeaderText="Configuration Name" SortExpression="strConfig" />
								<asp:BoundField DataField="strStoreName" HeaderText="Store Name" SortExpression="strStoreName" />
								<asp:BoundField DataField="strStoreEmail" HeaderText="Store Email" SortExpression="strStoreEmail" />
								<asp:BoundField DataField="strStoreContactNumber" HeaderText="Store Contact Number" SortExpression="strStoreContactNumber" />
								<asp:BoundField DataField="strUserName" HeaderText="User Name" SortExpression="strUserName" />
								<asp:BoundField DataField="strUpdatedTime" HeaderText="Updated Time" SortExpression="strUpdatedTime" />
								<asp:BoundField DataField="strIsUpdate" HeaderText="Update Status" SortExpression="strIsUpdate" />
								<asp:BoundField DataField="strIsActive" HeaderText="Service Status" SortExpression="strIsActive" />
							</Columns>
						</asp:GridView>
					</div>
					<asp:Button ID="btnEdit" runat="server" Text="Edit" OnClientClick="return RadioCheck();"
						data-toggle="modal" data-target="#myModalUsers"
						OnClick="btnEdit_Click" class="btn btn-primary" />
					<asp:Button ID="btnDelete" runat="server" Text="Delete" OnClientClick="return Validate();"
						OnClick="btnDelete_Click" class="btn btn-danger" />
				</div>
			</div>
			<div class="modal fade" id="myModalUsers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<h4 class="modal-title">Edit User Details</h4>
						</div>
						<div class="modal-body">
							<div class="row form-group">
								<div class="col-md-4 col-sm-6">SNo</div>
								<div class="col-md-6 col-sm-6">
									<asp:Label ID="lblid" runat="server"></asp:Label>
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4 col-sm-6">Selected Application</div>
								<div class="col-md-6 col-sm-6">
									<asp:TextBox ID="txtApplicationName" runat="server" ReadOnly="true" class="form-control" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4 col-sm-6">Selected configuration</div>
								<div class="col-md-6 col-sm-6">
									<asp:TextBox ID="txtConfigName" runat="server" ReadOnly="true" class="form-control" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4 col-sm-6">Store Name</div>
								<div class="col-md-6 col-sm-6">
									<asp:TextBox ID="txtStoreName" ToolTip="Enter only AlphaNumeric characters" runat="server" ReadOnly="true" class="form-control" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4 col-sm-6">UserID</div>
								<div class="col-md-6 col-sm-6">
									<asp:TextBox ID="txtuserid"  ReadOnly="true" runat="server" class="form-control" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4 col-sm-6">Store Email</div>
								<div class="col-md-6 col-sm-6">
									<asp:TextBox ID="txtStoreEmail" class="form-control" pattern="^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z][a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$" ToolTip="Enter Valid E-Mail" runat="server" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4 col-sm-6">Store Contact Number</div>
								<div class="col-md-6 col-sm-6">
									<asp:TextBox ID="txtStoreContactNumber"  class="form-control contactnumber" ToolTip="Enter only 10 digit Mobile Number" pattern="[789][0-9]{9}" runat="server" />
                                    <span class="contactnumberErr error-code"></span>
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4 col-sm-6">User Name</div>
								<div class="col-md-6 col-sm-6">
									<asp:TextBox ID="txtusername" runat="server" class="form-control username" />
                                    <span class="usernameErr error-code"></span>
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4 col-sm-6">Password</div>
								<div class="col-md-6 col-sm-6">
									<asp:TextBox ID="txtpwd" runat="server" class="form-control password" />
                                    <span class="passwordErr error-code"></span>
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4 col-sm-6">
									<asp:Label ID="lblupdateavail1" runat="server" Text="Update Status" Visible="false"></asp:Label>
								</div>
								<div class="col-md-6 col-sm-6">
									<asp:RadioButtonList ID="radList" runat="server" Width="300px" RepeatColumns="2" Visible="false" RepeatDirection="Horizontal" Height="20px">
										<asp:ListItem>Enable</asp:ListItem>
										<asp:ListItem>Disable</asp:ListItem>
									</asp:RadioButtonList>
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4 col-sm-6">
									<asp:Label ID="lblstatus" runat="server" Text="User Status" Visible="false"></asp:Label>
								</div>
								<div class="col-md-6 col-sm-6">
									<asp:RadioButtonList ID="raduserstatus" runat="server" Width="300px" RepeatColumns="2" Visible="false" RepeatDirection="Horizontal" Height="20px">
										<asp:ListItem>Active</asp:ListItem>
										<asp:ListItem>Inactive</asp:ListItem>
									</asp:RadioButtonList>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<asp:Button ID="btnUpdate" CommandName="Update" class="btn btn-primary updatevalidate" CausesValidation="false" runat="server" Text="Update" OnClick="btnUpdate_Click" />
							<asp:Button ID="btnCancel" runat="server" Text="Reset" CausesValidation="false" class="btn btn-default" OnClick="btnCancel_Click" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
