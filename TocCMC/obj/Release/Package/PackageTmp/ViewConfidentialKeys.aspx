﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ViewConfidentialKeys.aspx.cs" Inherits="TocCMC.ViewConfidentialKeys" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".confidentialtext").keyup(function () {
                var confidentialtext = $(".confidentialtext").val();    //gets the value of class confidentialtext
                if (confidentialtext == "")     //confidentialtext should not be empty
                {
                    $(".confidentialtextErr").text("please enter confidential text");
                    return false;
                }
                else {
                    $(".confidentialtextErr").text("");
                }
            });
            $(".updatevalidate").click(function () {
                var count = 0;
                var confidentialtext = $(".confidentialtext").val();
                if (confidentialtext == "")     //confidentialtext should not be empty
                {
                    $(".confidentialtextErr").text("please enter confidential text");
                    count = count + 1;
                }

                if (count > 0) {
                    return false;
                }
            });
        });

	   function Validate() {
		   var gv = document.getElementById("<%=gdviewcnfkey.ClientID%>");
			var rbs = gv.getElementsByTagName("input");
			var flag = 0;
			for (var i = 0; i < rbs.length; i++) {

				if (rbs[i].type == "radio") {
					if (rbs[i].checked) {
						flag = 1;
						break;
					}
				}
			}
			if (flag == 0) {
			    warningAlert('Please Select Anyone');
				return false;
			}
			else {
				var x = confirm("Are you sure you want to delete?");
				if (x == true)
					return true;
				else {
					if (document.getElementById("<%=Label1.ClientID%>") != null)
						 document.getElementById("<%=Label1.ClientID%>").innerText = "";
					 return false;
				 }
			 }
		 }
</script>
	  <script  type="text/javascript">
		  function SelectSingleRadiobutton(rdbtnid) {
			  var rdBtn = document.getElementById(rdbtnid);
			  var rdBtnList = document.getElementsByTagName("input");
			  for (i = 0; i < rdBtnList.length; i++) {
				  if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
					  rdBtnList[i].checked = false;
				  }
			  }
		  }
</script>
    <script>
        function ValidateFieldLegth(sender, args) {
            var v = document.getElementById('<%=txtsecreykey.ClientID%>').value;
        if (v.length < 6 && v.length > 10) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
        }
    }
</script>
	
    <div class="container col-xs-12">
        <div id="mainContainer">
            <div id="tblPanel">
                <div class="col-md-2 col-sm-6">
                    <div class="list-group">
                        <div class="list-group-item">
                            <asp:Button ID="btnNew" runat="server" Text="New Confidential Key" class="btn btn-primary" Style="width: 100%;font-size:10pt;"
                                OnClick="btnNew_Click" />
                            <asp:Label ID="Label1" runat="server"></asp:Label>
                        </div>
                        <div class="list-group-item">
                            <asp:DropDownList ID="ddlappname" AppendDataBoundItems="true" DataTextField="strAppName"
                                DataValueField="strSno" AutoPostBack="true" OnSelectedIndexChanged="ddlappname_SelectedIndexChanged"
                                runat="server" class="form-control">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-sm-6">
                    <div class="table-responsive">
                        <asp:GridView ID="gdviewcnfkey" runat="server" AutoGenerateColumns="false"
                            AllowSorting="true"
                            OnSorting="gdviewcnfkey_Sorting" class="table table-hover table-bordered table-striped">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:RadioButton ID="RadioButton1" runat="server" OnCheckedChanged="RadioButton1_CheckedChanged" AutoPostBack="true" OnClick="javascript:SelectSingleRadiobutton(this.id)" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="strSno" HeaderText="SNo" Visible="false" />
                                <asp:BoundField DataField="strAppName" HeaderText="Application Name" SortExpression="strAppName" />
                                <asp:BoundField DataField="strkey" HeaderText="Confidential Key" SortExpression="strkey" />
                                <asp:BoundField DataField="keyid" HeaderText="KeyID" SortExpression="keyid" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <asp:Button ID="btnEdit" runat="server" Text="Edit"
                        class="btn btn-primary" data-toggle="modal" data-target="#myModalCnfKey"
                        OnClick="btnEdit_Click" />
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClientClick="return Validate();" 
                        OnClick="btnDelete_Click" class="btn btn-danger"/>

                </div>
            </div>

            <div class="modal fade" id="myModalCnfKey" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Edit Confidential Text</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    SNo
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lblid" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Application Name
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtappname" runat="server" class="form-control" ReadOnly="true"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                     Key ID
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtkeyid" runat="server" ReadOnly="true" class="form-control" />
                                                                       
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                     Confidential Text
                                </div>
                                <div class="col-md-6 col-sm-6">
                                   <asp:TextBox ID="txtsecreykey" runat="server" class="form-control confidentialtext" />
                                    <span class="confidentialtextErr error-code"></span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnUpdate" CommandName="Update" runat="server" class="btn btn-primary updatevalidate" Text="Update" OnClick="btnUpdate_Click" CausesValidation="false" />
			                <asp:Button ID="btnReset" runat="server" Text="Reset" class="btn btn-default" 
                                 CausesValidation="false" OnClick="btnReset_Click" />
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </div>	
</asp:Content>
