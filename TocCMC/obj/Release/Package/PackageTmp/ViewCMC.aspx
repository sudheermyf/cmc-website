﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ViewCMC.aspx.cs" Inherits="TocCMC.ViewCMC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <script type="text/javascript">
        function Validate() {
            var gv = document.getElementById("<%=gdviewcmc.ClientID%>");
            var rbs = gv.getElementsByTagName("input");
            var flag = 0;
            for (var i = 0; i < rbs.length; i++) {

                if (rbs[i].type == "radio") {
                    if (rbs[i].checked) {
                        flag = 1;
                        break;
                    }
                }
            }
            if (flag == 0) {
                alert("Please Select Any One");
                return false;
            }
            else {
                var x = confirm("Are you sure you want to delete?");
                if (x == true)
                    return true;
               
             }
        }

    </script>
    <script type="text/javascript">
        function SelectSingleRadiobutton(rdbtnid) {
            var rdBtn = document.getElementById(rdbtnid);
            var rdBtnList = document.getElementsByTagName("input");
            for (i = 0; i < rdBtnList.length; i++) {
                if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                    rdBtnList[i].checked = false;
                }
            }
        }
    </script>

    <div class="container col-xs-12">
        <div id="mainContainer">            
            <div id="tblPanels">
                <div class="col-md-2 col-sm-6">
                    <div class="list-group">
                        <div class="list-group-item">
                            <asp:Button ID="btnNewCMC" runat="server" OnClick="btnNewCMC_Click" class="btn btn-primary" Style="width:100%;font-size:10pt;" Text="New CMC Configuration"/>
                           <%-- <asp:Label ID="Label1" runat="server"></asp:Label>--%>
                         
                        </div>
                        <div class="list-group-item">
                            <asp:DropDownList ID="ddlappname" AppendDataBoundItems="true" DataTextField="AppName"
                                DataValueField="SNo" AutoPostBack="true" OnSelectedIndexChanged="ddlappname_SelectedIndexChanged"
                                runat="server" class="form-control">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="list-group-item">
                            <asp:DropDownList ID="ddlcmc" AppendDataBoundItems="true" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlcmc_SelectedIndexChanged" runat="server" class="form-control">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-sm-6">
                    <div class="table-responsive">
                        <asp:GridView ID="gdviewcmc" runat="server" AutoGenerateColumns="False"
                            AllowSorting="true" OnSorting="gdviewcmc_Sorting"
                            class="table table-hover table-bordered table-striped"
                            OnRowDeleting="gdviewcmc_RowDeleting">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:RadioButton ID="RadioButton1" runat="server" OnCheckedChanged="RadioButton1_CheckedChanged"
                                            OnClick="javascript:SelectSingleRadiobutton(this.id)" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="strSno" HeaderText="SNo" Visible="false" />
                                <asp:BoundField DataField="strAppName" HeaderText="Application Name" SortExpression="strAppName" />
                                <asp:BoundField DataField="strConfigName" HeaderText="Configuration Name" SortExpression="strConfigName" />
                                <asp:BoundField DataField="strFtpUserName" HeaderText="FTP User Name" SortExpression="strFtpUserName" />
                                <asp:BoundField DataField="strUpdatedTime" HeaderText="Updated Time" SortExpression="strUpdatedTime" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <asp:Button ID="btnEditCMC" runat="server" Text="Edit" class="btn btn-primary" OnClick="btnEditCMC_Click1" />
                  <%--  <asp:Button ID="btnEditCMC" runat="server" class="btn btn-primary" data-toggle="modal"
                        data-target="#myModalViewCMC" OnClick="btnEditCMC_Click" Text="Edit"/>--%>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CausesValidation="false"
                        OnClientClick="return Validate();" OnClick="btnDelete_Click" class="btn btn-danger" />

                </div>
            </div>

            <div class="modal fade" id="myModalViewCMC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Edit CMC</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    SNo
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Label ID="lblid" runat="server" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Application Name
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtAppName" runat="server" class="form-control" disabled="disabled" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Configuration Name
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtConfigName" runat="server" class="form-control" disabled="disabled" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    FTP Path
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtftppath" class="form-control" required="required" runat="server" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    FTP UserName
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtftpusername" class="form-control" required="required" runat="server" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    FTP Password
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtftppwd" class="form-control" required="required" runat="server" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Client Location Drive
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtclientlocpath" class="form-control" required="required" runat="server" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Lauch Application Path
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtlaunchapppath" class="form-control" required="required" runat="server" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:CheckBox ID="chkActive" runat="server" Text="Is Active" Visible="false" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click" />
                            <asp:Button ID="btnCancel" runat="server" Text="Reset" CausesValidation="false" class="btn btn-default" OnClick="btnCancel_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
