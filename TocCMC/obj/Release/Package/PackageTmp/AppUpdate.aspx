﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="AppUpdate.aspx.cs" Inherits="TocCMC.AppUpdate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <script type="text/javascript">      
        function Validate() {
            var ddlappname = document.getElementById('<%=ddlappname.ClientID %>');
            var ddlappname1 = $(ddlappname).find('option').filter(':selected').text();
            //ddlappname app logic
            if (ddlappname1 == "Select" || ddlappname1 == "") {
                warningAlert('Select Application Name');
                $("#selectApp").attr("class", "row form-group has-error");
            }
            else {
                $("#selectApp").attr("class", "row form-group");
            }
            //logic to check all fields are entered or not.
            if (ddlappname1 != "Select" && ddlappname1 != "") {
                return true;
            }
            else {
                errorAlert('Select Required Fields');
                return false;
            }
        }
    </script>

     <div class="col-xs-10 col-md-6 col-sm-10 col-centered" style="margin-top: 3%;">
    <div class="panel panel-info">
        <div class="panel-heading">App Update</div>
        <div class="panel-body">

            <div class="row form-group" id="selectApp">
                <div class="col-md-4 col-sm-6">
                    Select Application Name
                </div>
                <div class="col-md-6 col-sm-6">
                    <asp:DropDownList ID="ddlappname" runat="server" DataTextField="AppName" DataValueField="SNo"
                         AutoPostBack="true" AppendDataBoundItems="true" class="form-control"
                         OnSelectedIndexChanged="ddlappname_SelectedIndexChanged">
                        <asp:ListItem Value="-1" Text="Select"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-4 col-sm-6">
                    
                </div>
                <div class="col-md-6 col-sm-6">
                    <asp:CheckBox ID="chkupdates" runat="server" Text="Updates Available" />
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-4 col-sm-6"></div>
                <div class="col-md-6 col-sm-6">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-primary"
                        OnClientClick="javascript:return Validate()" 
                         OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnReset" runat="server" Text="Reset" class="btn btn-default" OnClick="btnReset_Click" />
                </div>
            </div>
        </div>
    </div>
         </div>
</asp:Content>
