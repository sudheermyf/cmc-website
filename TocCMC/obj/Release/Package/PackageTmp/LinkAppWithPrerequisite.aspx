﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="LinkAppWithPrerequisite.aspx.cs" Inherits="TocCMC.LinkAppWithPrerequisite" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">      
        $(document).ready(function () {
            $(".ddlApp").change(function () {
                var value = $(".ddlApp").val();
                if (value == "Select") {
                    $(".ddlAppErr").text("please select application");
                    return false;
                }
                else {
                    $(".ddlAppErr").text("");
                }
            });
            $(".ddlStores").change(function () {
                var value = $(".ddlStores").val();
                if (value == "Select") {
                    $(".ddlStoresErr").text("please select store");
                    return false;
                }
                else {
                    $(".ddlStoresErr").text("");
                }
            });
            $(".ddlKiosk").change(function () {
                var value = $(".ddlKiosk").val();
                if (value == "Select") {
                    $(".ddlKioskErr").text("please select kiosk");
                    return false;
                }
                else {
                    $(".ddlKioskErr").text("");
                }
            });
            
            $(".validate").click(function () {
                debugger;
                var count = 0;
                var ddlApp = $(".ddlApp").val();
                var ddlStores = $(".ddlStores").val();
                var ddlKiosk = $(".ddlKiosk").val();
                if (ddlApp == "-1") {
                    $(".ddlAppErr").text("Please select application");
                    count = count + 1;
                }
                if (ddlStores == "-1") {
                    $(".ddlStoresErr").text("Please select store");
                    count = count + 1;
                }
                if (ddlKiosk == "-1") {
                    $(".ddlKioskErr").text("Please select kiosk");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>
    <div class="col-xs-10 col-md-6 col-sm-10 col-centered" style="margin-top: 3%;">
        <div class="panel panel-info">
            <div class="panel-heading">Link App With pre-Requisite</div>
            <div class="panel-body">

                <div class="row form-group" id="selectApp">
                    <div class="col-md-4 col-sm-6">
                        Select Application Name
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:DropDownList ID="ddlappname" runat="server" DataTextField="AppName" DataValueField="SNo"
                            AutoPostBack="true" AppendDataBoundItems="true" class="form-control ddlApp"
                            OnSelectedIndexChanged="ddlappname_SelectedIndexChanged">
                            <asp:ListItem Value="-1" Text="Select"></asp:ListItem>
                        </asp:DropDownList>
                        <span class="ddlAppErr error-code"></span>
                    </div>
                </div>
                  <div class="row form-group" id="selectStores">
                    <div class="col-md-4 col-sm-6">
                        Select Kiosk
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:DropDownList ID="ddlStores" runat="server" DataTextField="UserName" DataValueField="SNo"
                            AutoPostBack="true" AppendDataBoundItems="true" class="form-control ddlStores" OnSelectedIndexChanged="ddlStores_SelectedIndexChanged">
                            <asp:ListItem Value="-1" Text="Select"></asp:ListItem>
                        </asp:DropDownList>
                        <span class="ddlStoresErr error-code"></span>
                    </div>
                </div>
                 <div class="row form-group" id="selectKiosk">
                    <div class="col-md-4 col-sm-6">
                        Select Kiosk
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:DropDownList ID="ddlKiosk" runat="server" DataTextField="SysName" DataValueField="SNo"
                            AutoPostBack="true" AppendDataBoundItems="true" class="form-control ddlKiosk" OnSelectedIndexChanged="ddlKiosk_SelectedIndexChanged">
                            <asp:ListItem Value="-1" Text="Select"></asp:ListItem>
                        </asp:DropDownList>
                        <span class="ddlKioskErr error-code"></span>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-4 col-sm-6">
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:CheckBoxList ID="chkPrerequisite" runat="server" AppendDataBoundItems="true" DataTextField="Name" DataValueField="SNo"></asp:CheckBoxList>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-4 col-sm-6"></div>
                    <div class="col-md-6 col-sm-6">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-primary validate"
                            OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnReset" runat="server" Text="Reset" class="btn btn-default" OnClick="btnReset_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
