﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="CMCView.aspx.cs" Inherits="TocCMC.CMCView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">

         $(document).ready(function () {
             $(".ftppath").keyup(function () {
                 var ftppath = $(".ftppath").val();    //gets the value of class ftppath            
                 var ftpstartstring = ftppath.substring(0, 6);
                 if (ftppath == "" || ftppath == "ftp://")     //ftppath should not be empty
                 {
                     $(".ftppathErr").text("please enter ftp path");
                     $(".ftppath").val("ftp://");
                     return false;
                 }
                 if (ftpstartstring != "ftp://") {
                     $(".ftppathErr").text("please correct the path. 'eg: ftp://'");
                     $(".ftppath").val("ftp://");
                     return false;
                 }
                 else {
                     $(".ftppathErr").text("");
                 }
             });
             $(".username").keyup(function () {
                 var username = $(".username").val();    //gets the value of class username
                 if (username == "")     //username should not be empty
                 {
                     $(".usernameErr").text("please enter username name");
                     return false;
                 }
                 if (username.length > 40)   //username maximum length is 40 characters
                 {
                     $(".usernameErr").text("maximum 40 characters");
                     return false;
                 }
                 else {
                     $(".usernameErr").text("");
                 }
             });
             $(".password").keyup(function () {
                 var password = $(".password").val();    //gets the value of class password                
                 if (password == "")     //password should not be empty
                 {
                     $(".passwordErr").text("please enter password");
                     return false;
                 }
                 if (password.length < 6)    //password minimum length is 6 characters
                 {
                     $(".passwordErr").text("minimum 6 characters");
                     return false;
                 }
                 if (password.length > 16)   //password maximum length is 16 characters
                 {
                     $(".passwordErr").text("maximum 16 characters");
                     return false;
                 }
                 else {
                     $(".passwordErr").text("");
                 }
             });
             $(".locpath").keyup(function () {
                 var locpath = $(".locpath").val();    //gets the value of class locpath
                 if (locpath == "")     //locpath should not be empty
                 {
                     $(".locpathErr").text("please enter location path");
                     return false;
                 }
                 else {
                     $(".locpathErr").text("");
                 }
             });
             $(".apppath").keyup(function () {
                 var apppath = $(".apppath").val();    //gets the value of class apppath
                 if (apppath == "")     //apppath should not be empty
                 {
                     $(".apppathErr").text("please enter launch application path");
                     return false;
                 }
                 else {
                     $(".apppathErr").text("");
                 }
             });

             $(".updatevalidate").click(function () {
                 var count = 0;
                 var ftppath = $(".ftppath").val();
                 var username = $(".username").val();
                 var password = $(".password").val();
                 var locpath = $(".locpath").val();
                 var apppath = $(".apppath").val();
                 var ftpstartstring = ftppath.substring(0, 6);
                 if (ftpstartstring != "ftp://") {
                     $(".ftppathErr").text("please correct the path. 'eg: ftp://'");
                     $(".ftppath").val("ftp://");
                     count = count + 1;
                 }
                 if (ftppath == "" || ftppath == "ftp://")     //ftppath should not be empty
                 {
                     $(".ftppathErr").text("please enter ftp path");
                     $(".ftppath").val("ftp://");
                     count = count + 1;
                 }
                 if (username.length > 40)   //username maximum length is 40 characters
                 {
                     $(".usernameErr").text("maximum 40 characters");
                     count = count + 1;
                 }
                 if (username == "")     //username should not be empty
                 {
                     $(".usernameErr").text("please enter username name");
                     count = count + 1;
                 }
                 if (password.length < 6)    //password minimum length is 6 characters
                 {
                     $(".passwordErr").text("minimum 6 characters");
                     count = count + 1;
                 }
                 if (password.length > 16)   //password maximum length is 16 characters
                 {
                     $(".passwordErr").text("maximum 16 characters");
                     count = count + 1;
                 }
                 if (password == "")     //password should not be empty
                 {
                     $(".passwordErr").text("please enter password");
                     count = count + 1;
                 }
                 if (locpath == "") {
                     $(".locpathErr").text("please enter location path");
                     count = count + 1;
                 }
                 if (apppath == "") {
                     $(".apppathErr").text("please enter launch application path");
                     count = count + 1;
                 }
                 if (count > 0) {
                     return false;
                 }
             });
         });

         function Validate() {
             var gv = document.getElementById("<%=gdviewcmc.ClientID%>");
            var rbs = gv.getElementsByTagName("input");
            var flag = 0;
            for (var i = 0; i < rbs.length; i++) {

                if (rbs[i].type == "radio") {
                    if (rbs[i].checked) {
                        flag = 1;
                        break;
                    }
                }
            }
            if (flag == 0) {
                warningAlert('Please Select Anyone');
                return false;
            }
            else {
                var x = confirm("Are you sure you want to delete?");
                if (x == true)
                    return true;
            }
        }
    </script>
    <script type="text/javascript">
        function SelectSingleRadiobutton(rdbtnid) {
            var rdBtn = document.getElementById(rdbtnid);
            var rdBtnList = document.getElementsByTagName("input");
            for (i = 0; i < rdBtnList.length; i++) {
                if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                    rdBtnList[i].checked = false;
                }
            }
        }
    </script>

    <div class="container col-xs-12">
        <div id="mainContainer">
            <div id="tblPanel">
                <div class="col-md-2 col-sm-6">
                    <div class="list-group">
                        <div class="list-group-item">
                             <asp:Button ID="btnNewCMC" runat="server" OnClick="btnNewCMC_Click" class="btn btn-primary" Style="width:100%;font-size:10pt;" Text="New CMC Configuration"/>                          
                        </div>
                        <div class="list-group-item">
                            <asp:DropDownList ID="ddlappname" AppendDataBoundItems="true" DataTextField="AppName"
                                DataValueField="SNo" AutoPostBack="true" OnSelectedIndexChanged="ddlappname_SelectedIndexChanged"
                                runat="server" class="form-control">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="list-group-item">
                            <asp:DropDownList ID="ddlcmc" AppendDataBoundItems="true" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlcmc_SelectedIndexChanged" runat="server" class="form-control">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-sm-6">
                    <div class="table-responsive">
                        <asp:GridView ID="gdviewcmc" runat="server" AutoGenerateColumns="False"
                            AllowSorting="true" OnSorting="gdviewcmc_Sorting"
                            class="table table-hover table-bordered table-striped">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:RadioButton ID="RadioButton1" runat="server" OnCheckedChanged="RadioButton1_CheckedChanged"
                                            OnClick="javascript:SelectSingleRadiobutton(this.id)" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="strSno" HeaderText="SNo" Visible="false" />
                                <asp:BoundField DataField="strAppName" HeaderText="Application Name" SortExpression="strAppName" />
                                <asp:BoundField DataField="strConfigName" HeaderText="Configuration Name" SortExpression="strConfigName" />
                                <asp:BoundField DataField="strFtpUserName" HeaderText="FTP User Name" SortExpression="strFtpUserName" />
                                <asp:BoundField DataField="strUpdatedTime" HeaderText="Updated Time" SortExpression="strUpdatedTime" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <asp:Button ID="btnEditCMC" runat="server" Text="Edit" class="btn btn-primary" OnClick="btnEditCMC_Click" 
                        data-toggle="modal" data-target="#myModalViewCMC"/>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CausesValidation="false"
                        OnClientClick="return Validate();" OnClick="btnDelete_Click" class="btn btn-danger" />

                </div>
            </div>

            <div class="modal fade" id="myModalViewCMC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Edit CMC</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    SNo
                                </div>
                                <div class="col-md-6 col-sm-6">
                                     <asp:Label ID="lblid" runat="server" />
                                </div>
                            </div>
                            
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                    Application Name
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:TextBox ID="txtAppName" runat="server" class="form-control" ReadOnly="true"/>
                                </div>
                            </div>
                            
                          <div class="row form-group">
                                <div class="col-md-4 col-sm-6">Configuration Name                                 
                                </div>
                                <div class="col-md-6 col-sm-6"><asp:TextBox ID="txtConfigName" runat="server" class="form-control" ReadOnly="true"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4 col-sm-6">FTP Path
                                </div>
                                <div class="col-md-6 col-sm-6"><asp:TextBox ID="txtftppath" class="form-control ftppath" runat="server" />
                                <span class="ftppathErr error-code"></span>
                                </div>
                            </div>
                             <div class="row form-group">
                                <div class="col-md-4 col-sm-6">FTP UserName
                                </div>
                                <div class="col-md-6 col-sm-6"><asp:TextBox ID="txtftpusername" class="form-control username"  runat="server" />
                                <span class="usernameErr error-code"></span>
                                </div>
                            </div>

                             <div class="row form-group">
                                <div class="col-md-4 col-sm-6">FTP Password
                                </div>
                                <div class="col-md-6 col-sm-6"><asp:TextBox ID="txtftppwd" class="form-control password" runat="server" />
                                <span class="passwordErr error-code"></span>
                                </div>
                            </div>

                             <div class="row form-group">
                                <div class="col-md-4 col-sm-6">Client Location Drive
                                </div>
                                <div class="col-md-6 col-sm-6"> <asp:TextBox ID="txtclientlocpath" class="form-control locpath"  runat="server" />
                                <span class="locpathErr error-code"></span>
                                </div>
                            </div>

                             <div class="row form-group">
                                <div class="col-md-4 col-sm-6">Lauch Application Path
                                </div>
                                <div class="col-md-6 col-sm-6"><asp:TextBox ID="txtlaunchapppath" class="form-control apppath"  runat="server" />
                                <span class="apppathErr error-code"></span>
                                </div>
                            </div>

                             <div class="row form-group">
                                <div class="col-md-4 col-sm-6">
                                </div>
                                <div class="col-md-6 col-sm-6"><asp:CheckBox ID="chkActive" runat="server" Text="Is Active" Visible="false" />
                                </div>
                            </div>                                             
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update" class="btn btn-primary updatevalidate" OnClick="btnUpdate_Click"/>
                            <asp:Button ID="btnCancel" runat="server" Text="Reset" CausesValidation="false" class="btn btn-default"
                                 OnClick="btnCancel_Click" />
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

        </div>
    </div>
</asp:Content>
