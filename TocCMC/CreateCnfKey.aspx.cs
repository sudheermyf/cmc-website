﻿using TouchonCloudSecurityDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TocCMC
{
    public partial class CreateCnfKey : System.Web.UI.Page
    {
        object lockTarget = new object();
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    lock (lockTarget)
                    {
                        AppDB.Logger.Info("Initiating Create confidential Key.");
                        //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                        AppDB.Logger.Info("Checking for Atleast One Application Name exists in DB..");
                        if (AppDB.securityDB.RegApps.ToList().Count > 0)
                        {
                            lock (lockTarget)
                            {
                                AppDB.Logger.Info("Fetching all the applications and bind to the dropdownlist");
                                ddlappname.DataSource = AppDB.securityDB.RegApps.ToList();
                                ddlappname.DataBind();
                            }
                        }
                    }
                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtsecretkey.Text) && ddlappname.SelectedItem.Text != "Select")
                {
                    AppDB.Logger.Info("Initiating Create Confidential Key.");
                    AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                    if (AppDB.securityDB.RegApps.ToList().Exists(c => c.AppName.Equals(ddlappname.SelectedItem.Text, StringComparison.OrdinalIgnoreCase)))
                    {
                        RegisteredApplications selectedApplication = AppDB.securityDB.RegApps.ToList().Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                        if (selectedApplication.ConfidentialKey == null)
                        {
                            try
                            {
                                AppDB.Logger.Info("Checking for Confidential key exists in DB...");
                                if (!AppDB.securityDB.ConfidentialKeys.ToList().Exists(c => c.SecretKey.Equals(txtsecretkey.Text)))
                                {
                                    AppDB.Logger.Info("New Confidential key detected. Initiating Saving...");
                                    lock (lockTarget)
                                    {
                                        ConfidentialKey confKey = new ConfidentialKey();

                                        bool findFreshKey = true;
                                        while (findFreshKey)
                                        {
                                            long key = new Random().Next(1000, 99999);
                                            if (!AppDB.securityDB.ConfidentialKeys.ToList().Exists(c => c.KeyID.Equals(key)))
                                            {
                                                findFreshKey = false;
                                                confKey.KeyID = key;
                                            }
                                        }
                                        confKey.SecretKey = txtsecretkey.Text;
                                        selectedApplication.ConfidentialKey = confKey;
                                        AppDB.securityDB.SaveChanges();
                                        clearAll();
                                        ClientScript.RegisterStartupScript(GetType(), "notifier", "successAlert('Confidential Key Created Successfully');", true);
                                        
                                        
                                    }
                                    AppDB.Logger.Info("Saved.");
                                }
                            }
                            catch (Exception ex)
                            {
                                AppDB.Logger.ErrorException(ex.Message, ex);
                                ClientScript.RegisterStartupScript(GetType(), "notifier2", "errorAlert('" + ex.Message + "');", true);
                            }
                        }
                        else
                        {
                            AppDB.Logger.Info("Already Confidential key exist.");
                            ddlappname.ClearSelection();
                            ddlappname.Items.FindByText("Select").Selected = true;
                            txtsecretkey.Text = "";
                            ClientScript.RegisterStartupScript(GetType(), "notifier3", "errorAlert('Already confidential key exist for this application');", true);
                        }
                    }
                    else
                    {
                        AppDB.Logger.Info("Invalid Registered Application.");
                        ClientScript.RegisterStartupScript(GetType(), "notifier4", "errorAlert('Invalid Registered Application');", true);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "notifier5", "errorAlert('Required Fileds should not be Empty');", true);
                }

            }
            catch (Exception ex)
            {
                AppDB.Logger.ErrorException(ex.Message, ex);
                ClientScript.RegisterStartupScript(GetType(), "notifier6", "errorAlert('" + ex.Message + "');", true);
            }

        }
        private void clearAll()
        {
            ddlappname.ClearSelection();
            ddlappname.Items.FindByText("Select").Selected = true;
            txtsecretkey.Text = "";
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            clearAll();
            ClientScript.RegisterStartupScript(GetType(), "notifier7", "messageAlert('Data has been reset');", true);
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewConfidentialKeys.aspx");
        }
    }
}