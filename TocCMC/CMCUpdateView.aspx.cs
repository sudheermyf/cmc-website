﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TocCMC
{
    public partial class CMCUpdateView : System.Web.UI.Page
    {
        object lockTarget = new object();
        DataTable dt;
        DataRow dr;
        
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    lock (lockTarget)
                    {
                        AppDB.Logger.Info("Initiating View CMC.");
                        //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                        AppDB.Logger.Info("Checking for Atleast One Configuration exists in DB..");
                        if (AppDB.securityDB.CMCUpdates.ToList().Count > 0)
                        {
                            GetTotalData();
                        }
                    }
                }
            }

        }
        protected void btnNewCMC_Click(object sender, EventArgs e)
        {
            Response.Redirect("CMCUpdateConfig.aspx");
        }
        private void GetTotalData()
        {
            lock (lockTarget)
            {
                dt = new DataTable();
                dr = null;
                dt.Columns.Add(new DataColumn("strSno", typeof(long)));
                dt.Columns.Add(new DataColumn("strVersion", typeof(int)));
                dt.Columns.Add(new DataColumn("strConfigName", typeof(string)));
                dt.Columns.Add(new DataColumn("strFtpUserName", typeof(string)));
                dt.Columns.Add(new DataColumn("strUpdatedTime", typeof(string)));
                foreach (var item in AppDB.securityDB.CMCUpdates.ToList())
                {
                    long strSno = item.SNo;
                    TouchonCloudSecurityDB.Models.CMCUpdateConfiguration cmcconfig = AppDB.securityDB.CMCUpdates.Where(c => c.SNo.Equals(strSno)).FirstOrDefault();
                    if (cmcconfig != null)
                    {
                        string strConfigName = cmcconfig.ConfigurationName;
                        string strFtpUserName = cmcconfig.FtpUserName;
                        string strUpdatedTime = cmcconfig.UpdatedTime.ToString();
                        int version = cmcconfig.CMCVersion;
                        dr = dt.NewRow();
                        dr["strSno"] = strSno;
                        dr["strVersion"] = version;
                        dr["strConfigName"] = strConfigName;
                        dr["strFtpUserName"] = strFtpUserName;
                        dr["strUpdatedTime"] = strUpdatedTime;
                        dt.Rows.Add(dr);
                        ViewState["CurrentTable"] = dt;

                    }
                }
                AppDB.Logger.Info("Fetching all the cmc configuration  from DB to the GridView");
                gdviewcmc.DataSource = dt;
                gdviewcmc.DataBind();

            }
        }
        protected void btnEditCMC_Click(object sender, EventArgs e)
        {
            bool isChecked = false;
            foreach (GridViewRow row in gdviewcmc.Rows)
            {
                RadioButton rb = (RadioButton)row.FindControl("RadioButton1");
                if (rb.Checked)
                {
                    isChecked = true;
                }
            }

            if (!isChecked)
            {
                ClientScript.RegisterStartupScript(GetType(), "notifier1", "warningAlert('Please Select Anyone');", true);
            }
            else
            {
                System.Text.StringBuilder sbj = new System.Text.StringBuilder();
                sbj.Append("<script type='text/javascript'>");
                sbj.Append("$('#myModalViewCMC').modal('show');");
                sbj.Append("</script>");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", sbj.ToString(), false);
            }
        }

        private void GetSelectedItemData()
        {
            long id = Convert.ToInt64(lblid.Text);
            TouchonCloudSecurityDB.Models.CMCUpdateConfiguration cmc = AppDB.securityDB.CMCUpdates.Where(c => c.SNo.Equals(id)).FirstOrDefault();
            txtConfigName.Text = cmc.ConfigurationName;
            txtftppath.Text = cmc.FtpPath;
            txtftppwd.Text = cmc.FtpPassword;
            txtCMCVersion.Text = cmc.CMCVersion.ToString();
            txtftpusername.Text = cmc.FtpUserName;
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            //Label1.Visible = false;
            long id = 0;
            int Index;
            DataTable dt = (DataTable)ViewState["CurrentTable"];

            for (int i = 0; i < gdviewcmc.Rows.Count; i++)
            {
                GridViewRow row = gdviewcmc.Rows[i];
                RadioButton rb = (RadioButton)row.FindControl("RadioButton1");
                if (rb.Checked)
                {
                    Index = row.RowIndex;
                    //int strI = dt.Rows.IndexOf(dt.Rows[Index]);
                    string strId = dt.Rows[Index]["strSno"].ToString();
                    try
                    {
                        id = Convert.ToInt64(strId);
                        AppDB.Logger.Info("Initiating Delete Configuration ID: " + id);
                        TouchonCloudSecurityDB.Models.CMCUpdateConfiguration updateConfig = AppDB.securityDB.CMCUpdates.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                        if (updateConfig != null)
                        {
                            List<TouchonCloudSecurityDB.Models.StoreAdmin> storeAdmin = AppDB.securityDB.StoreAdmin.Where(c => c.CMCConfig.ConfigurationName.Equals(updateConfig.ConfigurationName)).ToList();

                            if (storeAdmin == null)
                            {
                                AppDB.securityDB.CMCUpdates.Remove(updateConfig);
                                AppDB.securityDB.SaveChanges();
                                GetTotalData();
                                ClientScript.RegisterStartupScript(GetType(), "notifier2", "successAlert('CMC Configuration Deleted Successfully');", true);
                                
                                
                                GetTotalData();
                            }
                            else if (storeAdmin.Count == 0)
                            {
                                AppDB.securityDB.CMCUpdates.Remove(updateConfig);
                                AppDB.securityDB.SaveChanges();
                                GetTotalData();
                                ClientScript.RegisterStartupScript(GetType(), "notifier3", "successAlert('CMC Configuration Deleted Successfully');", true);
                                
                                
                                GetTotalData();
                            }
                            else
                            {
                                ClientScript.RegisterStartupScript(GetType(), "notifier4", "warningAlert('First Delete Users related to this CMC Configuration');", true);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        AppDB.Logger.ErrorException(ex.Message, ex);
                        ClientScript.RegisterStartupScript(GetType(), "notifier5", "errorAlert('" + ex.Message + "');", true);
                    }
                }
            }
        }
        protected void gdviewcmc_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dataTable = ViewState["CurrentTable"] as DataTable;
            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression;
                gdviewcmc.DataSource = dataView;
                gdviewcmc.DataBind();
            }
        }
        protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            int Index;
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            for (int i = 0; i < gdviewcmc.Rows.Count; i++)
            {
                GridViewRow row = gdviewcmc.Rows[i];
                RadioButton rdb = (RadioButton)row.FindControl("RadioButton1");
                if (rdb.Checked == true)
                {
                    lock (lockTarget)
                    {
                        Index = row.RowIndex;
                        lblid.Text = dt.Rows[Index]["strSno"].ToString();
                        long id = Convert.ToInt64(lblid.Text);
                        TouchonCloudSecurityDB.Models.CMCUpdateConfiguration cmc = AppDB.securityDB.CMCUpdates.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                        txtConfigName.Text = cmc.ConfigurationName;
                        txtftppath.Text = cmc.FtpPath;
                        txtftppwd.Text = cmc.FtpPassword;
                        txtftpusername.Text = cmc.FtpUserName;
                        txtCMCVersion.Text = cmc.CMCVersion.ToString();
                    }
                }
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            AppDB.Logger.Info("Initiating Application Configuration ID: " + lblid.Text + " Configuration Name: " + txtConfigName.Text);
            if (!string.IsNullOrEmpty(txtConfigName.Text) && !string.IsNullOrEmpty(txtftppwd.Text) && !string.IsNullOrEmpty(txtftppath.Text) && !string.IsNullOrEmpty(txtftpusername.Text))
            {
                lock (lockTarget)
                {
                    AppDB.Logger.Info("Application Config validating completed.");
                    long id = Convert.ToInt64(lblid.Text);
                    TouchonCloudSecurityDB.Models.CMCUpdateConfiguration updateConfig = AppDB.securityDB.CMCUpdates.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                    if (updateConfig != null)
                    {
                       
                            if (AppDB.securityDB.CMCUpdates.ToList().Exists(c => c.ConfigurationName.Equals(txtConfigName.Text, StringComparison.OrdinalIgnoreCase)))
                            {
                                try
                                {
                                    updateConfig.FtpPassword = txtftppwd.Text.Trim();
                                    updateConfig.FtpPath = txtftppath.Text.Trim();
                                    updateConfig.FtpUserName = txtftpusername.Text.Trim();
                                    updateConfig.ConfigurationName = txtConfigName.Text.Trim();
                                    updateConfig.CMCVersion  = Convert.ToInt32(txtCMCVersion.Text);
                                    updateConfig.UpdatedTime = DateTime.Now;
                                    AppDB.securityDB.SaveChanges();
                                    GetTotalData();
                                    ClientScript.RegisterStartupScript(GetType(), "notifier6", "successAlert('Configuration Updated Successfully');", true);
                                    
                                    
                                }
                                catch (Exception ex)
                                {
                                    AppDB.Logger.ErrorException(ex.Message, ex);
                                    ClientScript.RegisterStartupScript(GetType(), "notifier7", "errorAlert('" + ex.Message + "');", true);
                                }
                            }

                       
                    }
                    else
                    {
                        AppDB.Logger.Info("Configuration id not found.");
                        ClientScript.RegisterStartupScript(GetType(), "notifier9", "errorAlert('Configuration id not found');", true);
                    }
                }
            }
            else
            {
                AppDB.Logger.Info("Invalid Configuration.");
                ClientScript.RegisterStartupScript(GetType(), "notifier10", "errorAlert('Invalid Configuration');", true);
            }

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            lock (lockTarget)
            {
                GetSelectedItemData();
                System.Text.StringBuilder sbj = new System.Text.StringBuilder();
                sbj.Append("<script type='text/javascript'>");
                sbj.Append("$('#myModalViewCMC').modal('show');");
                sbj.Append("</script>");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", sbj.ToString(), false);
            }
        }
    }
}